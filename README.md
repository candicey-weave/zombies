# Zombies
- A [Weave](https://github.com/Weave-MC) mod for improving Hypixel Zombies experience.

<br>

## Features
**Strategy Viewer**
* Display strategy from pastebin or URL.

**Cornering**
* Hide players in range.

**Hologram Fixer**
* Help you to not accidentally buy things when you are trying to shoot.

**DPS Counter**
* Display your damage per second.

**Auto Splits**
* An accurate timer for each round.

**Wave Time**
* Display all start times of each wave and highlight the current one.

**SLA**
* Display information about active windows based on your coordinates.

**Not Enough Zombies**
* See [here](https://github.com/PingIsFun/NotEnoughZombies).

*All settings are available in OneConfig GUI, which can be opened by pressing `RSHIFT`, and the Control Menu.*

<br>

## Images
![Showcase](assets/image/Showcase.png)  
**Some settings GUIs**  
![Cornering](assets/image/ZombiesCornering.png)  
![Strategy Viewer](assets/image/ZombiesStrategyViewer.png)

<br>

## Installation
1. Download the [Zombies](#download) mod.
2. Place the jar in your Weave mods directory.
    1. Windows: `%userprofile%\.weave\mods`
    2. Unix: `~/.weave/mods`

<br>

## Download
- [Package Registry](https://gitlab.com/candicey-weave/zombies/-/packages) - Select the latest version and download the jar file that ended with `-relocated.jar`.

<br>

## Build
- Clone the repository.
- Run `./gradlew build` in the root directory.
- The built jar file will be in `build/libs`.

<br>

## Credits
- [Weave MC](https://github.com/Weave-MC) - For the mod loader.
- [syeyound](https://github.com/cyoung06) - Inspiration for strategy viewer, cornering, and hologram fixer.
- [PingIsFun](https://github.com/PingIsFun) - For NotEnoughZombies.
- [Stachelbeere1248](https://github.com/Stachelbeere1248) - For SLA, and waves data.
- [tahmid-23](https://github.com/tahmid-23) - For Auto Splits.

<br>

## License
- Zombies is licensed under the [GNU General Public License Version 3](https://gitlab.com/candicey-weave/zombies/-/blob/master/LICENSE).