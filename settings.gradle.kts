rootProject.name = "Zombies"

pluginManagement {
    repositories {
        gradlePluginPortal()
        maven("https://jitpack.io")
    }
}

include("Zenith-Core")