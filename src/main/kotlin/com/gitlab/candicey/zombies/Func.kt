package com.gitlab.candicey.zombies

import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.gui.OneConfigGui
import cc.polyfrost.oneconfig.gui.animations.EaseInOutQuad
import cc.polyfrost.oneconfig.utils.Notifications
import cc.polyfrost.oneconfig.utils.gui.GuiUtils
import com.gitlab.candicey.zenithcore.HOME
import net.weavemc.loader.api.event.EventBus
import java.io.File

internal fun info(message: String) = LOGGER.info("[Zombies] $message")

internal fun warn(message: String) = LOGGER.warn("[Zombies] $message")
internal fun warn(message: String, throwable: Throwable) = LOGGER.warn("[Zombies] $message", throwable)

fun resolveConfigFile(name: String): File = File(HOME, ".weave/Zombies/$name").apply { parentFile?.mkdirs() }

fun refreshOneConfigGui() {
    OneConfigGui.INSTANCE?.let(EventBus::unsubscribe)
    OneConfigGui.INSTANCE = null
}

fun sendNotification(title: String, message: String, duration: Float = 4000f, progressBar: Boolean = true) {
    val easeInOutQuad = EaseInOutQuad(duration.toInt(), 0f, 1f, false)
    if (progressBar) {
        Notifications.INSTANCE.send(title, message, -1f, { easeInOutQuad.get(GuiUtils.getDeltaTime()) }, null)
    } else {
        Notifications.INSTANCE.send(title, message, duration, null, null)
    }
}

fun brightness(colour: OneColor, amount: Float): OneColor {
    val r = colour.red * amount
    val g = colour.green * amount
    val b = colour.blue * amount
    return OneColor(r.toInt(), g.toInt(), b.toInt(), colour.alpha)
}