package com.gitlab.candicey.zombies

import cc.polyfrost.oneconfig.config.core.OneColor
import com.gitlab.candicey.zenithcore.command.CommandInitialisationData
import com.gitlab.candicey.zenithcore.command.CommandManager
import com.gitlab.candicey.zenithcore.config.Config
import com.gitlab.candicey.zenithcore.config.ConfigManager
import com.gitlab.candicey.zombies.config.HiddenModsConfig
import com.gitlab.candicey.zombies.extension.addZPrefix
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

internal val LOGGER: Logger = LogManager.getLogger("Zombies")

internal val COMMAND_PREFIX = listOf("Zombies", "Zombie", "Z")
internal const val PREFIX = "/"

internal val commandInitialisationData by lazy {
    CommandInitialisationData(COMMAND_PREFIX, PREFIX, String::addZPrefix)
}
internal val commandManager by lazy { CommandManager(commandInitialisationData) }

internal val configManager by lazy {
    ConfigManager(
        mutableMapOf(
            "hidden mods" to Config(resolveConfigFile("hidden-mods.json"), HiddenModsConfig()),
        )
    )
}
internal val hiddenModsConfig by lazy { configManager.getConfig<HiddenModsConfig>()!! }

val whiteColour by lazy { OneColor(0xFFFFFFFF.toInt()) }