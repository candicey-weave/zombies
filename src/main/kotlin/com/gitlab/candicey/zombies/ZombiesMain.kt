package com.gitlab.candicey.zombies

import cc.polyfrost.oneconfig.events.EventManager
import com.gitlab.candicey.zenithcore.extension.add
import com.gitlab.candicey.zenithcore.helper.HookManagerHelper
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zenithloader.ZenithLoader
import com.gitlab.candicey.zenithloader.dependency.Dependencies.CONCENTRA
import com.gitlab.candicey.zenithloader.dependency.Dependencies.ZENITH_CORE
import com.gitlab.candicey.zenithloader.dependency.config
import com.gitlab.candicey.zombies.command.impl.AdvancedDebugCommand
import com.gitlab.candicey.zombies.command.impl.HiddenModulesCommand
import com.gitlab.candicey.zombies.command.impl.ZombiesStrategyViewerCommand
import com.gitlab.candicey.zombies.command.impl.notenoughzombies.NotEnoughZombiesCommand
import com.gitlab.candicey.zombies.command.impl.sla.SlaCommand
import com.gitlab.candicey.zombies.config.ZombiesDamagePerSecondCounterConfig
import com.gitlab.candicey.zombies.hook.*
import com.gitlab.candicey.zombies.mod.autosplits.AutoSplits
import com.gitlab.candicey.zombies.mod.cornering.ZombiesCornering
import com.gitlab.candicey.zombies.mod.damagepersecondcounter.ZombiesDamagePerSecondCounter
import com.gitlab.candicey.zombies.mod.hologramfixer.ZombiesHologramFixer
import com.gitlab.candicey.zombies.mod.modlist.ModList
import com.gitlab.candicey.zombies.mod.notenoughzombies.*
import com.gitlab.candicey.zombies.mod.sharp.Sharp
import com.gitlab.candicey.zombies.mod.sla.ZombiesSLA
import com.gitlab.candicey.zombies.mod.strategyviewer.ZombiesStrategyViewer
import com.gitlab.candicey.zombies.mod.wavetime.WaveTime
import com.gitlab.candicey.zombies.util.ScoreboardUtil
import net.weavemc.loader.api.ModInitializer
import net.weavemc.loader.api.event.EventBus
import net.weavemc.loader.api.event.StartGameEvent

class ZombiesMain : ModInitializer {
    override fun preInit() {
        info("Initialising...")

        ZenithLoader.loadDependencies(
            // zombies.versions.json
            ZENITH_CORE config "zombies",
            CONCENTRA config "zombies",
        )

        HookManagerHelper.hooks.add(
            NetworkManagerHook,
            GuiInGameHook,
            EntityLivingBaseHook,
            RendererLivingEntityHook,
            NotEnoughZombiesConfigHook,
        )

        EventBus.subscribe(StartGameEvent.Post::class.java) { init() }

        commandManager.registerCommand(
            ZombiesStrategyViewerCommand,
            NotEnoughZombiesCommand,
            SlaCommand,
            AdvancedDebugCommand,
            HiddenModulesCommand,
        )

        info("Initialised!")
    }

    private fun init() {
        configManager.init()

        runAsync(ZombiesStrategyViewer::calculateLines)

        arrayOf(
            ZombiesCornering,

            ZombiesHologramFixer,

            ZombiesDamagePerSecondCounter,

            NotEnoughZombiesTitleEventListener,
            NotEnoughZombiesChatReceivedEventListener,
            NotEnoughZombiesLivingUpdateEventListener,
            NotEnoughZombiesEntityListAddEventListener,
            NotEnoughZombiesEventDebugger,
            ZombiesGame,
            PowerUpAlert,
            PowerUpCountdown,
            HideMessages,

            AutoSplits,

            ZombiesSLA.Handler,

            Sharp,

            ScoreboardUtil,
        ).forEach(EventBus::subscribe)

        arrayOf(
            ModList,
            ZombiesStrategyViewer,
            AutoSplits,
            WaveTime,
            ZombiesSLA.Handler,
        ).forEach(EventManager.INSTANCE::register)

        ZombiesDamagePerSecondCounterConfig.initialize()
    }
}