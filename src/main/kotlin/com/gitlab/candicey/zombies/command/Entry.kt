package com.gitlab.candicey.zombies.command

import com.gitlab.candicey.zenithcore.command.CommandAbstract
import com.gitlab.candicey.zombies.commandInitialisationData

abstract class Entry : CommandAbstract(commandInitialisationData)