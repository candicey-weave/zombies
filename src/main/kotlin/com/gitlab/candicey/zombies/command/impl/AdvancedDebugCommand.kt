package com.gitlab.candicey.zombies.command.impl

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.command.HideCommand
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.extension.addZPrefix

fun advancedDebugOperation(operation: () -> Unit) {
    if (AdvancedDebugCommand.advancedDebug) {
        operation()
    }
}

@HideCommand
@CommandInfo("advanceddebug")
object AdvancedDebugCommand : Entry() {
    var advancedDebug = false

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        advancedDebug = !advancedDebug

        "Advanced debug: $advancedDebug"
            .addZPrefix()
            .toChatComponent()
            .addChatMessage()
    }
}