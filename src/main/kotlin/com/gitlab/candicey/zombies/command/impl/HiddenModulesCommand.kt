package com.gitlab.candicey.zombies.command.impl

import com.gitlab.candicey.zenithcore.command.CommandAbstract
import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.command.HideCommand
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.DARK_AQUA
import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.extension.addZPrefix
import com.gitlab.candicey.zombies.hiddenModsConfig

@HideCommand
@CommandInfo("hiddenmodules")
object HiddenModulesCommand : Entry() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            SharpConfigHiddenModulesCommand,
        )

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        checkArgs(args, previousCommandName)
        runSubCommand = true
    }

    @CommandInfo("sharp")
    object SharpConfigHiddenModulesCommand : Entry() {
        private val sharpReference = hiddenModsConfig.config.enableDisable::sharp

        override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
            if (args.isEmpty()) {
                printState()
                return
            }

            val type = Type.fromString(args[0])
            when (type) {
                Type.ENABLE -> {
                    sharpReference.set(true)
                    printState()
                }

                Type.DISABLE -> {
                    sharpReference.set(false)
                    printState()
                }

                Type.TOGGLE -> {
                    sharpReference.set(!sharpReference.get())
                    printState()
                }

                null -> {
                    "Invalid type. Valid types are: ${Type.values().joinToString("$YELLOW, ") { "${DARK_AQUA}${it.name.lowercase()}" }}$YELLOW."
                        .addZPrefix()
                        .toChatComponent()
                        .addChatMessage()
                }
            }

            hiddenModsConfig.writeConfig()
        }

        private fun printState() {
            "${YELLOW}Sharp config is ${if (sharpReference.get()) "${GREEN}enabled" else "${RED}disabled"}${YELLOW}."
                .addZPrefix()
                .toChatComponent()
                .addChatMessage()
        }
    }

    private enum class Type {
        ENABLE,
        DISABLE,
        TOGGLE;

        companion object {
            fun fromString(string: String): Type? = values().find { it.name.equals(string, true) }
        }
    }
}