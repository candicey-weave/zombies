package com.gitlab.candicey.zombies.command.impl

import com.gitlab.candicey.zenithcore.command.CommandAbstract
import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer.SetLineCommand
import com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer.SetLineCountCommand
import com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer.SetStrategy

@CommandInfo("strategyviewer", "zombiesstrategyviewer", "ZSV", "SV")
object ZombiesStrategyViewerCommand : Entry() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            SetStrategy,
            SetLineCommand,
            SetLineCountCommand,
        )
}