package com.gitlab.candicey.zombies.command.impl.notenoughzombies

import com.gitlab.candicey.zenithcore.command.CommandAbstract
import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zombies.command.Entry

@CommandInfo("notenoughzombies", "nez")
object NotEnoughZombiesCommand : Entry() {
    override val subCommands: MutableList<CommandAbstract>
        get() = mutableListOf(
            SetPowerUpRoundCommand,
            RollCommand,
        )
}