package com.gitlab.candicey.zombies.command.impl.notenoughzombies

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.GOLD
import com.gitlab.candicey.zenithcore.util.WHITE
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.extension.addNEZPrefix
import com.gitlab.candicey.zombies.mod.notenoughzombies.ZombiesGame

@CommandInfo("roll")
object RollCommand : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        "Lucky Chest Roll Data:"
            .addNEZPrefix()
            .toChatComponent()
            .addChatMessage()

        for ((key, innerMap) in ZombiesGame.luckyChestRollData) {
            val result = StringBuilder()

            // Indentation based on the level
            result.append(GOLD).append(key).append(": ")
            var c = 0
            for ((key1, value) in innerMap.entries) {
                // Indentation for subentries
                result.append(WHITE)
                    .append(key1)
                    .append(": ")
                    .append(value)

                if (c >= innerMap.size - 1) {
                    break
                }

                result.append(", ")

                c++
            }

            result
                .toString()
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()
        }
    }
}