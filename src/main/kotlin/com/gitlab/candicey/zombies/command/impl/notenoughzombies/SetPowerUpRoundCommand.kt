package com.gitlab.candicey.zombies.command.impl.notenoughzombies

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zombies.COMMAND_PREFIX
import com.gitlab.candicey.zombies.PREFIX
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.data.PowerUp
import com.gitlab.candicey.zombies.extension.addNEZPrefix
import com.gitlab.candicey.zombies.mod.notenoughzombies.ZombiesGame
import com.gitlab.candicey.zombies.util.ZombiesUtil

@CommandInfo("setpowerupround", "spr", "sp")
object SetPowerUpRoundCommand : Entry() {
    private val powerUpCommandMap = mapOf(
        "ik" to PowerUp.INSTA_KILL,
        "mx" to PowerUp.MAX_AMMO,
        "ss" to PowerUp.SHOPPING_SPREE,
    )

    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        fun sendUsage() =
            "Usage: $PREFIX${COMMAND_PREFIX[0]} ${previousCommandName.joinToString(" ")} ${aliases[0]} <power up name> <round>"
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()

        if (args.size < 2) {
            sendUsage()
            return
        }
        val powerUpName = args[0]
        val round = args[1].toIntOrNull()
        if (round == null) {
            sendUsage()
            return
        }

        val powerUp = powerUpCommandMap.get(powerUpName)
        if (powerUp == null) {
            "Invalid power up name."
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()
            return
        }

        if (round == -1) {
            ZombiesGame.resetPowerUpPattern(powerUp)
            return
        }

        val map = ZombiesUtil.getMap()
        if (map == null) {
            "Could not find the current map."
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()
            return
        }
        val powerUpPatternNumOption = powerUp.getPatternNumber(map, round)
        if (powerUpPatternNumOption == null) {
            "Power up pattern not found."
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()
            return
        }

        ZombiesGame.setPowerUpPattern(powerUp, powerUpPatternNumOption)
    }
}