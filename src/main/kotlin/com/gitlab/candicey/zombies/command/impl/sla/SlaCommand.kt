package com.gitlab.candicey.zombies.command.impl.sla

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zombies.COMMAND_PREFIX
import com.gitlab.candicey.zombies.PREFIX
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.data.Map
import com.gitlab.candicey.zombies.extension.addSLAPrefix
import com.gitlab.candicey.zombies.mod.sla.ZombiesSLA

@CommandInfo("sla")
object SlaCommand : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        checkArgs(args, previousCommandName)

        val mapName = args[0]
        if (mapName == "clear") {
            ZombiesSLA.clearMap()
            return
        }

        val map = Map.fromMapName(mapName)
        if (map == null) {
            "Invalid map name: $mapName"
                .addSLAPrefix()
                .toChatComponent()
                .addChatMessage()
            return
        }

        ZombiesSLA.changeMap(map)
    }

    override fun sendHelpMessage(previousCommandName: List<String>) {
        "$PREFIX${COMMAND_PREFIX[0]} ${previousCommandName.joinToString(" ")} ${aliases[0]} <de|bb|aa|clear>"
            .addSLAPrefix()
            .toChatComponent()
            .addChatMessage()
    }
}