package com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.DARK_AQUA
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.extension.addZSVPrefix
import com.gitlab.candicey.zombies.mod.strategyviewer.ZombiesStrategyViewer

@CommandInfo("setline", "sl")
object SetLineCommand : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            "Please specify a line number.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        val line = args[0].toIntOrNull()?.let { it - 1 } ?: run {
            "Invalid line number.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        if (line < 0) {
            "Line number must be greater than 0.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        ZombiesStrategyViewer.run {
            if (line >= renderedText.size) {
                "Line number must be less than or equal to $DARK_AQUA${renderedText.size}$YELLOW.".addZSVPrefix().toChatComponent().addChatMessage()
                return
            }

            currentLine = line
            "Line has been set to $DARK_AQUA${line + 1}$YELLOW.".addZSVPrefix().toChatComponent().addChatMessage()
        }
    }
}