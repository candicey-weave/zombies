package com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.DARK_AQUA
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.config.ZombiesHologramFixerConfig
import com.gitlab.candicey.zombies.config.ZombiesStrategyViewerConfig
import com.gitlab.candicey.zombies.extension.addZSVPrefix

@CommandInfo("setlinecount", "slc")
object SetLineCountCommand : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            "Please specify a line count.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        val lineCount = args[0].toIntOrNull()
        if (lineCount == null) {
            "Invalid line count.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        "Line count has been set to $DARK_AQUA$lineCount$YELLOW.".addZSVPrefix().toChatComponent().addChatMessage()
        ZombiesStrategyViewerConfig.showLineCount = lineCount
        ZombiesHologramFixerConfig.save()
    }
}