package com.gitlab.candicey.zombies.command.impl.zombiesstrategyviewer

import com.gitlab.candicey.zenithcore.command.CommandInfo
import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zombies.command.Entry
import com.gitlab.candicey.zombies.config.ZombiesStrategyViewerConfig
import com.gitlab.candicey.zombies.extension.addZSVPrefix
import com.gitlab.candicey.zombies.mod.strategyviewer.ZombiesStrategyViewer

@CommandInfo("setstrategy", "setstrat", "ss")
object SetStrategy : Entry() {
    override fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            ZombiesStrategyViewerConfig.strategy = ""
            ZombiesStrategyViewerConfig.save()
            runAsync { ZombiesStrategyViewer.calculateLines(notifyError = true, notifySuccess = true) }
            "Line cleared.".addZSVPrefix().toChatComponent().addChatMessage()
            return
        }

        ZombiesStrategyViewerConfig.strategy = args[0]
        ZombiesStrategyViewerConfig.save()
        runAsync { ZombiesStrategyViewer.calculateLines(notifyError = true, notifySuccess = true) }
    }
}