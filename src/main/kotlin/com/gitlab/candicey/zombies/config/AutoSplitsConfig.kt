package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.*
import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_AUTO_SPLITS
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_LIVE_SPLIT
import com.gitlab.candicey.zombies.extension.addDependencyAll
import com.gitlab.candicey.zombies.extension.addListener
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.mod.autosplits.AutoSplits
import com.gitlab.candicey.zombies.sendNotification
import com.gitlab.candicey.zombies.whiteColour
import org.lwjgl.input.Keyboard

object AutoSplitsConfig : Config(Mod("Zombies Auto Splits", ModType.HYPIXEL), "zombies-autosplits.json") {
    @DualOption(
        name = "Splitter",
        description = "The splitter to use.",
        left = "Internal",
        right = "LiveSplit",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_AUTO_SPLITS,
    )
    var liveSplit: Boolean = false

    @Text(
        name = "Address",
        description = "The address of the livesplit server.",
        subcategory = CATEGORY_LIVE_SPLIT,
    )
    var address: String = "localhost"

    @Text(
        name = "Port",
        description = "The port of the livesplit server.",
        subcategory = CATEGORY_LIVE_SPLIT,
    )
    var port: String = "16834"

    @Button(
        name = "Reload LiveSplit Config",
        text = "Reload",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_LIVE_SPLIT,
    )
    val reloadLiveSplit: Runnable = Runnable(AutoSplits::reloadSplitter)

    @Slider(
        name = "Font Size",
        min = 0f,
        max = 200.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var fontSize: Int = 20

    @Slider(
        name = "Text Margin X",
        min = 0f,
        max = 5000.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textMarginX: Int = 0

    @Slider(
        name = "Text Margin Y",
        min = 0f,
        max = 5000.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textMarginY: Int = 0

    @Color(
        name = "Text Color",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textColour: OneColor = whiteColour

    @Checkbox(
        name = "Text Shadow",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textShadow: Boolean = true

    @Dropdown(
        name = "Position",
        options = ["Top Left", "Top Right", "Bottom Left", "Bottom Right"],
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var position: Int = 0

    @KeyBind(
        name = "Toggle Auto Splits",
        size = OptionSize.DUAL,
        subcategory = Constant.CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        addDependencyAll(
            ::address,
            ::port,
            ::reloadLiveSplit,

            ::liveSplit,
        )

        addListener(::liveSplit, AutoSplits::reloadSplitter)

        registerToggleKeyBind(toggleKeyBind)
    }

    fun checkAndGetPort(): Int? =
        port.toIntOrNull().also {
            if (it == null) {
                sendNotification(
                    "AutoSplits",
                    "Invalid port. The port must be a number between 0 and 65535."
                )
            }
        }
}