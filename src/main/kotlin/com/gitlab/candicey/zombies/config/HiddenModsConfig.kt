package com.gitlab.candicey.zombies.config

data class HiddenModsConfig(
    val enableDisable: EnableDisable = EnableDisable(),
) {
    data class EnableDisable(
        var sharp: Boolean = false,
    )
}