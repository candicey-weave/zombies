package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.*
import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_APPEARANCE
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_KEYBINDS
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.extension.reopenGuiOperation
import com.gitlab.candicey.zombies.mod.modlist.ModList
import com.gitlab.candicey.zombies.whiteColour
import org.lwjgl.input.Keyboard

object ModListConfig : Config(Mod("Zombies Mod List", ModType.HYPIXEL), "zombies-modlist.json") {
    @Slider(
        name = "Font Size",
        min = 0f,
        max = 200.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var fontSize: Int = 20

    @Slider(
        name = "Text Margin X",
        min = 0f,
        max = 5000.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var textMarginX: Int = 0

    @Slider(
        name = "Text Margin Y",
        min = 0f,
        max = 5000.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var textMarginY: Int = 0

    @Color(
        name = "Text Colour",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textColour: OneColor = whiteColour

    @Checkbox(
        name = "Text Shadow",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textShadow: Boolean = true

    @Color(
        name = "Enabled Text Colour",
        subcategory = CATEGORY_APPEARANCE,
    )
    var enabledTextColour: OneColor = ModList.State.ON.colour

    @Button(
        name = "Reset Enabled Text Colour",
        text = "Reset",
        subcategory = CATEGORY_APPEARANCE,
    )
    val resetEnabledTextColour: Runnable = Runnable {
        reopenGuiOperation {
            enabledTextColour = ModList.State.ON.colour
        }
    }

    @Color(
        name = "Disabled Text Colour",
        subcategory = CATEGORY_APPEARANCE,
    )
    var disabledTextColour: OneColor = ModList.State.OFF.colour

    @Button(
        name = "Reset Disabled Text Colour",
        text = "Reset",
        subcategory = CATEGORY_APPEARANCE,
    )
    val resetDisabledTextColour: Runnable = Runnable {
        reopenGuiOperation {
            disabledTextColour = ModList.State.OFF.colour
        }
    }

    @Dropdown(
        name = "Position",
        options = ["Top Left", "Top Right", "Bottom Left", "Bottom Right"],
        subcategory = CATEGORY_APPEARANCE,
    )
    var position: Int = 0

    @KeyBind(
        name = "Toggle Mod List",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }
}
