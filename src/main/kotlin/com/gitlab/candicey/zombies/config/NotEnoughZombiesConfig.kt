package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.Dropdown
import cc.polyfrost.oneconfig.config.annotations.Number
import cc.polyfrost.oneconfig.config.annotations.Switch
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_CHAT
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_DEBUG
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_HIDE_MESSAGES

/**
 * The main Config entrypoint that extends the Config type and inits the config options.
 * See [this link](https://docs.polyfrost.cc/oneconfig/config/adding-options) for more config Options
 */
object NotEnoughZombiesConfig : Config(Mod("Not Enough Zombies", ModType.HYPIXEL), "not-enough-zombies.json", false) {
    /* GENERAL */
    @Dropdown(
        name = "Alert on power up spawn",
        options = ["Off", "Self", "Party", CATEGORY_CHAT],
    )
    var powerUpSpawnAlert: Int = 1

    @Dropdown(
        name = "Alert on power up despawn",
        options = ["Off", "Self", "Party", CATEGORY_CHAT],
    )
    var powerUpDeSpawnAlert: Int = 1

    @Dropdown(
        name = "Next power up round alert",
        options = ["Off", "Self", "Party", CATEGORY_CHAT],
    )
    var nextPowerUpRoundAlert: Int = 1

    @Switch(
        name = "Show despawn countdown next to the power up",
    )
    var powerUpCountdown: Boolean = false

    /* CHAT */
    // Hide Messages subcategory
    @Switch(
        name = "Hide Gold Received Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideGoldMessages: Boolean = false

    @Switch(
        name = "Hide Window Repair Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideWindowRepairMessages: Boolean = false

    @Switch(
        name = "Hide Revive Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideReviveMessages: Boolean = false

    @Switch(
        name = "Hide Knockdown Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideKnockdownMessages: Boolean = false

    @Switch(
        name = "Hide Target Hit Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideHitTargetMessages: Boolean = false

    @Switch(
        name = "Hide Lucky Chest Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideLuckyChestMessages: Boolean = false

    @Switch(
        name = "Hide Open Area Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hideOpenAreaMessages: Boolean = false

    @Switch(
        name = "Hide Player Leave/Rejoin  Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hidePlayerConnectionStatusMessages: Boolean = false

    @Switch(
        name = "Hide Power Up Pickup Messages",
        category = CATEGORY_CHAT,
        subcategory = CATEGORY_HIDE_MESSAGES,
    )
    var hidePowerUpPickupMessages: Boolean = false

    @Switch(
        name = "Show Zombies's events",
        category = CATEGORY_DEBUG,
    )
    var debugEvents: Boolean = false

    @Number(
        name = "Notification time",
        min = 1f,
        max = 60000f,
        category = CATEGORY_DEBUG,
    )
    var debugEventnotificationDuration: Float = 2000f

    init {
        initialize()
    }
}
