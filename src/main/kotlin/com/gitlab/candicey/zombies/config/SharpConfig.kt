package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.KeyBind
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.hiddenModsConfig
import org.lwjgl.input.Keyboard

private val sharpConfigInstance by lazy { SharpConfig() }
val sharpConfig: SharpConfig?
    get() = if (hiddenModsConfig.config.enableDisable.sharp) sharpConfigInstance else null

class SharpConfig : Config(Mod("Zombies Sharp", ModType.HYPIXEL), "zombies-sharp.json", false) {
    @KeyBind(
        name = "Toggle Sharp",
        size = OptionSize.DUAL,
        subcategory = Constant.CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }
}