package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.*
import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.InfoType
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.extension.reopenGuiOperation
import org.lwjgl.input.Keyboard

object WaveTimeConfig : Config(Mod("Zombies Wave Time", ModType.HYPIXEL), "zombies-wavetime.json") {
    @Exclude
    @JvmStatic
    @Info(
        text = "Auto Splits with Internal Splitter selected must be enabled for this module to work correctly.",
        type = InfoType.WARNING,
        size = OptionSize.DUAL,
    )
    val infoText: Boolean = false

    @Slider(
        name = "Font Size",
        min = 0f,
        max = 200.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var fontSize: Int = 20

    @Slider(
        name = "Text Margin X",
        min = 0f,
        max = 5000.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textMarginX: Int = 0

    @Slider(
        name = "Text Margin Y",
        min = 0f,
        max = 5000.0f,
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var textMarginY: Int = 0

    @Color(
        name = "Inactive Wave Text Colour",
        description = "The colour of the text for inactive waves.",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var inactiveWaveTextColour: OneColor = Colour.INACTIVE_WAVE_TEXT_COLOUR

    @Checkbox(
        name = "Inactive Wave Text Shadow",
        description = "Whether or not the text for inactive waves should have a shadow.",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var inactiveWaveTextShadow: Boolean = true

    @Color(
        name = "Active Wave Text Colour",
        description = "The colour of the text for active waves.",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var activeWaveTextColour: OneColor = Colour.ACTIVE_WAVE_TEXT_COLOUR

    @Checkbox(
        name = "Active Wave Text Shadow",
        description = "Whether or not the text for active waves should have a shadow.",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var activeWaveTextShadow: Boolean = true

    @Button(
        name = "Reset Active Wave Text Colour",
        text = "Reset Active",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    val resetActiveWaveTextColour: Runnable = Runnable {
        reopenGuiOperation {
            activeWaveTextColour = Colour.ACTIVE_WAVE_TEXT_COLOUR
        }
    }

    @Button(
        name = "Reset Inactive Wave Text Colour",
        text = "Reset Inactive",
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    val resetInactiveWaveTextColour: Runnable = Runnable {
        reopenGuiOperation {
            inactiveWaveTextColour = Colour.INACTIVE_WAVE_TEXT_COLOUR
        }
    }

    @Dropdown(
        name = "Position",
        options = ["Top Left", "Top Right", "Bottom Left", "Bottom Right"],
        subcategory = Constant.CATEGORY_APPEARANCE,
    )
    var position: Int = 0

    @KeyBind(
        name = "Toggle Wave Time",
        size = OptionSize.DUAL,
        subcategory = Constant.CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }

    private object Colour {
        val INACTIVE_WAVE_TEXT_COLOUR = OneColor(0xFF5A5A5A.toInt())
        val ACTIVE_WAVE_TEXT_COLOUR = OneColor(0xFFFFFF00.toInt())
    }
}