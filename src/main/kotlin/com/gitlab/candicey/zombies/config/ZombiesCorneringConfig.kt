package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.KeyBind
import cc.polyfrost.oneconfig.config.annotations.Slider
import cc.polyfrost.oneconfig.config.annotations.Switch
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_CORNERING
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_KEYBINDS
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import org.lwjgl.input.Keyboard

object ZombiesCorneringConfig : Config(Mod("Zombies Cornering", ModType.HYPIXEL), "zombies-cornering.json", false) {
    @Slider(
        name = "Distance",
        min = 0.0f,
        max = 500f,
        step = 1,
        subcategory = CATEGORY_CORNERING,
    )
    var distance: Float = 7f

    @Switch(
        name = "Show Self",
        description = "Whether or not to show your player entity.",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_CORNERING,
    )
    var showSelf: Boolean = false

    @KeyBind(
        name = "Toggle Cornering",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }
}
