package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.HUD
import cc.polyfrost.oneconfig.config.annotations.KeyBind
import cc.polyfrost.oneconfig.config.annotations.Slider
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import cc.polyfrost.oneconfig.hud.SingleTextHud
import com.gitlab.candicey.zombies.data.Constant
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_DAMAGE_PER_SECOND_COUNTER
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.mod.damagepersecondcounter.ZombiesDamagePerSecondCounter
import org.lwjgl.input.Keyboard

object ZombiesDamagePerSecondCounterConfig : Config(Mod("Zombies DPS Counter", ModType.HYPIXEL), "zombies-dps.json") {
    @Slider(
        name = "Update Interval",
        description = "How often the DPS counter updates. (in ticks)",
        min = 1f,
        max = 20f,
        step = 1,
        subcategory = CATEGORY_DAMAGE_PER_SECOND_COUNTER,
    )
    var updateInterval: Int = 1

    @HUD(
        name = "Hud",
    )
    var dpsHud: DpsHud = DpsHud()

    @KeyBind(
        name = "Toggle DPS Counter",
        size = OptionSize.DUAL,
        subcategory = Constant.CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        registerToggleKeyBind(toggleKeyBind)
    }

    class DpsHud : SingleTextHud("DPS", true) {
        override fun getText(example: Boolean): String =
            String.format("%.2f", ZombiesDamagePerSecondCounter.count())
    }
}