package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.KeyBind
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_KEYBINDS
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import org.lwjgl.input.Keyboard

object ZombiesHologramFixerConfig : Config(Mod("Zombies Hologram Fixer", ModType.HYPIXEL), "zombies-hologramfixer.json") {
    @KeyBind(
        name = "Toggle Hologram Fixer",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }
}
