package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.*
import cc.polyfrost.oneconfig.config.annotations.Number
import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zombies.data.Constant
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_APPEARANCE
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_OFFSET
import com.gitlab.candicey.zombies.enum.RotationDirection
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.mod.sla.sla
import com.gitlab.candicey.zombies.whiteColour
import org.lwjgl.input.Keyboard

object ZombiesSlaConfig : Config(Mod("Zombies SLA", ModType.HYPIXEL), "zombies-sla.json", false) {
    @Number(
        name = "X Offset",
        min = 0f,
        max = 5000f,
        subcategory = CATEGORY_OFFSET,
    )
    var offsetX: Float = 0f

    @Slider(
        name = "Y Offset",
        min = 0f,
        max = 5000f,
        subcategory = CATEGORY_OFFSET,
    )
    var offsetY: Float = 0f

    @Slider(
        name = "Z Offset",
        min = 0f,
        max = 5000f,
        subcategory = CATEGORY_OFFSET,
    )
    var offsetZ: Float = 0f

    @Button(
        name = "Rotate Clockwise",
        text = "Rotate",
        subcategory = CATEGORY_OFFSET,
    )
    val rotateClockWise: Runnable = Runnable {
        sla?.rotate(RotationDirection.CLOCKWISE)
    }

    @Button(
        name = "Rotate Counter Clockwise",
        text = "Rotate",
        subcategory = CATEGORY_OFFSET,
    )
    val rotateCounterClockWise: Runnable = Runnable {
        sla?.rotate(RotationDirection.COUNTER_CLOCKWISE)
    }

    @Button(
        name = "Mirror X",
        text = "Mirror",
        subcategory = CATEGORY_OFFSET,
    )
    val mirrorX: Runnable = Runnable {
        sla?.mirrorX()
    }

    @Button(
        name = "Mirror Z",
        text = "Mirror",
        subcategory = CATEGORY_OFFSET,
    )
    val mirrorZ: Runnable = Runnable {
        sla?.mirrorZ()
    }

    @Color(
        name = "Text Color",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textColour: OneColor = whiteColour

    @Checkbox(
        name = "Text Shadow",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textShadow: Boolean = true

    @Slider(
        name = "Font Size",
        min = 0f,
        max = 200.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var fontSize: Int = 20

    @Slider(
        name = "Text Margin X",
        min = 0f,
        max = 5000.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var textMarginX: Int = 0

    @Slider(
        name = "Text Margin Y",
        min = 0f,
        max = 5000.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var textMarginY: Int = 0

    @Dropdown(
        name = "Position",
        options = ["Top Left", "Top Right", "Bottom Left", "Bottom Right"],
        subcategory = CATEGORY_APPEARANCE,
    )
    var position: Int = 0

    @KeyBind(
        name = "Toggle SLA",
        size = OptionSize.DUAL,
        subcategory = Constant.CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    init {
        initialize()

        registerToggleKeyBind(toggleKeyBind)
    }
}