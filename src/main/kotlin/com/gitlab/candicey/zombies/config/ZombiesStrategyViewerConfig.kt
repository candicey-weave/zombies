package com.gitlab.candicey.zombies.config

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.annotations.*
import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import cc.polyfrost.oneconfig.config.data.Mod
import cc.polyfrost.oneconfig.config.data.ModType
import cc.polyfrost.oneconfig.config.data.OptionSize
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_APPEARANCE
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_KEYBINDS
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_STRATEGY
import com.gitlab.candicey.zombies.extension.addListener
import com.gitlab.candicey.zombies.extension.registerToggleKeyBind
import com.gitlab.candicey.zombies.mod.strategyviewer.ZombiesStrategyViewer
import com.gitlab.candicey.zombies.whiteColour
import org.lwjgl.input.Keyboard

object ZombiesStrategyViewerConfig : Config(Mod("Zombies Strategy Viewer", ModType.HYPIXEL), "zombies-strategyviewer.json", false) {
    @Text(
        name = "Strategy",
        description = "The URL of the strategy to display.",
        placeholder = "https://pastebin.com/raw/...",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_STRATEGY,
    )
    var strategy: String = ""

    @Button(
        name = "Refresh Strategy",
        text = "Refresh",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_STRATEGY,
    )
    val refreshStrategy: Runnable = Runnable {
        runAsync { ZombiesStrategyViewer.calculateLines(notifyError = true, notifySuccess = true, ignoreFirstlyCalculatedTextIsBlankNotification = true) }
    }

    @Slider(
        name = "Show Line Count",
        min = 0f,
        max = 100.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var showLineCount: Int = 15

    @Slider(
        name = "Maximum Characters Per Line",
        min = 0f,
        max = 500.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var maximumCharactersPerLine: Int = 70

    @Slider(
        name = "New Line Height",
        min = 0f,
        max = 100f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var newLineHeight: Int = 45

    @Slider(
        name = "Font Size",
        min = 0f,
        max = 200.0f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var fontSize: Int = 20

    @Color(
        name = "Text Color",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textColour: OneColor = whiteColour

    @Checkbox(
        name = "Text Shadow",
        subcategory = CATEGORY_APPEARANCE,
    )
    var textShadow: Boolean = true

    @Color(
        name = "Line Count Color",
        subcategory = CATEGORY_APPEARANCE,
    )
    var lineCountColour: OneColor = whiteColour

    @Checkbox(
        name = "Line Count Shadow",
        subcategory = CATEGORY_APPEARANCE,
    )
    var lineCountShadow: Boolean = false

    @Color(
        name = "Background Color",
        subcategory = CATEGORY_APPEARANCE,
    )
    var backgroundColour: OneColor = OneColor(0x55FFFFFF)

    @Slider(
        name = "Background Corner Radius",
        min = 0f,
        max = 20f,
        subcategory = CATEGORY_APPEARANCE,
    )
    var backgroundCornerRadius: Int = 10

    @KeyBind(
        name = "Toggle Strategy Viewer",
        size = OptionSize.DUAL,
        subcategory = CATEGORY_KEYBINDS,
    )
    var toggleKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_NONE)

    @KeyBind(
        name = "Scroll Up",
        description = "Scroll up 1 line, or hold ALT to scroll up 10 lines.",
        subcategory = CATEGORY_KEYBINDS,
    )
    var scrollUpKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_UP)

    @KeyBind(
        name = "Scroll Down",
        description = "Scroll down 1 line, or hold ALT to scroll down 10 lines.",
        subcategory = CATEGORY_KEYBINDS,
    )
    var scrollDownKeyBind: OneKeyBind = OneKeyBind(Keyboard.KEY_DOWN)

    init {
        initialize()

        addListener(::maximumCharactersPerLine) {
            ZombiesStrategyViewer.calculateLines(refreshCachedText = false)
        }
        addListener("enabled") {
            with(ZombiesStrategyViewer) {
                if (!firstlyCalculated) {
                    calculateLines(ignoreFirstlyCalculatedTextIsBlankNotification = true)
                }
            }
        }

        registerToggleKeyBind(toggleKeyBind)

        fun isAltDown() = Keyboard.isKeyDown(Keyboard.KEY_LMENU) || Keyboard.isKeyDown(Keyboard.KEY_RMENU)

        registerKeyBind(scrollUpKeyBind) {
            if (!enabled) {
                return@registerKeyBind
            }

            with(ZombiesStrategyViewer) {
                currentLine = (currentLine - if (isAltDown()) 10 else 1).coerceAtLeast(0)
            }
        }

        registerKeyBind(scrollDownKeyBind) {
            if (!enabled) {
                return@registerKeyBind
            }

            with(ZombiesStrategyViewer) {
                currentLine = (currentLine + if (isAltDown()) 10 else 1).coerceAtMost(maximumCurrentLine)
            }
        }
    }
}
