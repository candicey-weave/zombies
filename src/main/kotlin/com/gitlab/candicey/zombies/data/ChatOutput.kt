package com.gitlab.candicey.zombies.data

enum class ChatOutput(val chatTypeString: String) {
    OFF("Off"),
    SELF("Self"),
    PARTY("Party"),
    CHAT("Chat");

    companion object {
        fun fromName(name: String): ChatOutput =
            values().find { it.name.equals(name, true) }
                ?: OFF

        fun fromIndex(index: Int): ChatOutput =
            values().find { it.ordinal == index }
                ?: OFF
    }
}