package com.gitlab.candicey.zombies.data

object Constant {
    const val CATEGORY_DEBUG = "Debug"

    const val CATEGORY_APPEARANCE = "Appearance"

    const val CATEGORY_KEYBINDS = "Keybinds"

    const val CATEGORY_CORNERING = "Cornering"

    const val CATEGORY_STRATEGY = "Strategy"

    const val CATEGORY_DAMAGE_PER_SECOND_COUNTER = "Damage Per Second Counter"

    const val CATEGORY_OFFSET = "Offset"

    const val CATEGORY_CHAT = "Chat"

    const val CATEGORY_HIDE_MESSAGES = "Hide Messages"

    const val CATEGORY_AUTO_SPLITS = "Auto Splits"

    const val CATEGORY_LIVE_SPLIT = "Live Split"
}
