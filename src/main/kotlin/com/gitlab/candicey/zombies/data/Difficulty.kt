package com.gitlab.candicey.zombies.data

enum class Difficulty {
    NORMAL,
    HARD,
    RIP,
}