package com.gitlab.candicey.zombies.data

import com.gitlab.candicey.zombies.data.Difficulty.*
import com.gitlab.candicey.zombies.data.Map.*

enum class GameMode(val map: Map, val difficulty : Difficulty) {
    DEAD_END_NORMAL(DEAD_END, NORMAL),
    DEAD_END_HARD(DEAD_END, HARD),
    DEAD_END_RIP(DEAD_END, RIP),

    BAD_BLOOD_NORMAL(BAD_BLOOD, NORMAL),
    BAD_BLOOD_HARD(BAD_BLOOD, HARD),
    BAD_BLOOD_RIP(BAD_BLOOD, RIP),

    ALIEN_ARCADIUM_NORMAL(ALIEN_ARCADIUM, NORMAL);

    fun changeCurrentDifficulty(difficulty: Difficulty) {
        currentGameMode = when (difficulty) {
            NORMAL -> when (map) {
                DEAD_END -> DEAD_END_NORMAL
                BAD_BLOOD -> BAD_BLOOD_NORMAL
                ALIEN_ARCADIUM -> ALIEN_ARCADIUM_NORMAL
            }

            HARD -> when (map) {
                DEAD_END -> DEAD_END_HARD
                BAD_BLOOD -> BAD_BLOOD_HARD
                ALIEN_ARCADIUM -> ALIEN_ARCADIUM_NORMAL
            }

            RIP -> when (map) {
                DEAD_END -> DEAD_END_RIP
                BAD_BLOOD -> BAD_BLOOD_RIP
                ALIEN_ARCADIUM -> ALIEN_ARCADIUM_NORMAL
            }
        }
    }

    companion object {
        var currentGameMode : GameMode? = null
            private set

        fun changeCurrentMap(map: Map) {
            currentGameMode = when (map) {
                DEAD_END -> DEAD_END_NORMAL
                BAD_BLOOD -> BAD_BLOOD_NORMAL
                ALIEN_ARCADIUM -> ALIEN_ARCADIUM_NORMAL
            }
        }

        fun clearCurrentGameMode() {
            currentGameMode = null
        }
    }
}
