package com.gitlab.candicey.zombies.data

sealed class LineTypes {
    object Empty : LineTypes()

    object NewLine : LineTypes()

    data class Text(val text: String) : LineTypes()
}