package com.gitlab.candicey.zombies.data

enum class Map(val mapName: String, val shortName: String) {
    DEAD_END("Dead End", "de"),
    BAD_BLOOD("Bad Blood", "bb"),
    ALIEN_ARCADIUM("Alien Arcadium", "aa");

    companion object {
        fun fromMapName(mapName: String): Map? =
            values().find { it.mapName.equals(mapName, true) || it.shortName.equals(mapName, true) }
    }
}