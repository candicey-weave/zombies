package com.gitlab.candicey.zombies.data

import com.gitlab.candicey.zombies.data.PowerUpPatternData.AlienArcadium
import com.gitlab.candicey.zombies.data.PowerUpPatternData.BadBlood
import com.gitlab.candicey.zombies.data.PowerUpPatternData.DeadEnd
import java.util.*

typealias MapPattern = List<SortedSet<Int>>

enum class PowerUp(val fullName: String, val shortName: String) {
    INSTA_KILL("Insta Kill", "IK", AlienArcadium.INSTA_KILL, DeadEnd.INSTA_KILL, BadBlood.INSTA_KILL),

    MAX_AMMO("Max Ammo", "MA", AlienArcadium.MAX_AMMO, DeadEnd.MAX_AMMO, BadBlood.MAX_AMMO),

    SHOPPING_SPREE("Shopping Spree", "SS", AlienArcadium.SHOPPING_SPREE, null, null),

    DOUBLE_GOLD("Double Gold", "DG"),

    CARPENTER("Carpenter", "CA"),

    BONUS_GOLD("Bonus Gold", "BG");

    val patternMap = EnumMap<Map, List<SortedSet<Int>>?>(Map::class.java)

    constructor(
        fullName: String,
        shortName: String,
        deadEndPattern: MapPattern,
        badBloodPattern: MapPattern?,
        alienArcadiumPattern: MapPattern?
    ) : this(fullName, shortName) {
        patternMap[Map.DEAD_END] = deadEndPattern
        patternMap[Map.BAD_BLOOD] = badBloodPattern
        patternMap[Map.ALIEN_ARCADIUM] = alienArcadiumPattern
    }

    fun getPattern(map: Map): MapPattern? = patternMap[map]

    fun hasPattern(map: Map): Boolean = getPattern(map) != null

    fun getPatternNumber(map: Map, round: Int): Int? {
        val pattern = getPattern(map) ?: return null

        for (i in pattern.indices step 2) {
            val powerUpPattern = pattern[i]
            if (powerUpPattern.contains(round)) {
                return i
            }
        }

        return null
    }

    fun getNextPowerUpRound(map: Map, currentRound: Int, patternNum: Int): Int? {
        if (patternNum % 2 != 0) {
            return null
        }

        val patternData = getPattern(map) ?: return null
        if (patternData.size < patternNum + 1) {
            return null
        }

        val powerUpPattern = patternData[patternNum]
        val digits = patternData[patternNum + 1]
        val biggestPatternNum = powerUpPattern.max()
        if (biggestPatternNum >= currentRound) {
            return findNumberAfter(powerUpPattern, currentRound)
        }
        if (digits.isEmpty()) {
            return null
        }

        var tensDown = currentRound - currentRound % 10
        var res: Int
        for (i in 0..9) {
            for (digit in digits) {
                res = tensDown + digit
                if (res >= currentRound) {
                    return res
                }
            }
            tensDown += 10
        }

        return null
    }

    private fun findNumberAfter(numbers: SortedSet<Int>, target: Int): Int? = numbers.firstOrNull { it >= target }

    companion object {
        fun fromName(name: String): PowerUp? = values().find { it.fullName.equals(name, true) }
    }
}