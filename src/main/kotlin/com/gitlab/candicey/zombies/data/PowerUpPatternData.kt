package com.gitlab.candicey.zombies.data

import java.util.*

object PowerUpPatternData {
    object AlienArcadium {
        val INSTA_KILL = listOf(
            TreeSet(mutableListOf(2, 5, 8, 11, 14, 17, 20, 23)), Collections.emptySortedSet(),
            TreeSet(mutableListOf(3, 6, 9, 12, 15, 18, 21)), Collections.emptySortedSet()
        )

        val MAX_AMMO = listOf<SortedSet<Int>>(
            TreeSet(mutableListOf(2, 5, 8, 12, 16)), TreeSet(mutableListOf(1, 6)),
            TreeSet(mutableListOf(3, 6, 9, 13, 17)), TreeSet(mutableListOf(2, 7))
        )

        val SHOPPING_SPREE = listOf<SortedSet<Int>>(
            TreeSet(mutableListOf(5, 15, 45)), TreeSet(setOf(5)),
            TreeSet(setOf(6)), TreeSet(setOf(6)),
            TreeSet(setOf(7)), TreeSet(setOf(7))
        )
    }

    object DeadEnd {
        val INSTA_KILL = listOf(
            TreeSet(mutableListOf(2, 8, 11, 14, 17, 23)), Collections.emptySortedSet(),
            TreeSet(mutableListOf(3, 6, 9, 12, 18, 21, 24)), Collections.emptySortedSet()
        )

        val MAX_AMMO = listOf(
            TreeSet(mutableListOf(2, 8, 12, 16, 21, 26)), Collections.emptySortedSet(),
            TreeSet(mutableListOf(3, 6, 9, 13, 17, 22, 27)), Collections.emptySortedSet()
        )
    }

    object BadBlood {
        val INSTA_KILL = listOf(
            TreeSet(mutableListOf(2, 5, 8, 11, 14, 17, 23)), Collections.emptySortedSet(),
            TreeSet(mutableListOf(3, 6, 9, 12, 18, 21, 24)), Collections.emptySortedSet()
        )

        val MAX_AMMO = listOf(
            TreeSet(mutableListOf(2, 5, 8, 12, 16, 21, 26)), Collections.emptySortedSet(),
            TreeSet(mutableListOf(3, 6, 9, 13, 17, 22, 27)), Collections.emptySortedSet()
        )
    }
}