package com.gitlab.candicey.zombies.data

data class Room(
    val alias: String,
    val windows: List<Window>,
) {
    var activeWindowCount: Int = 0

    fun increaseActiveWindowCount() {
        activeWindowCount++
    }

    fun resetActiveWindowCount() {
        activeWindowCount = 0
    }

    companion object {
        val deadEnd by lazy {
            listOf(
                Room(
                    "Alley",
                    listOf(
                        Window(1, 13, 138, 63),
                        Window(2, 9, 138, 87),
                        Window(3, 79, 140, 17),
                        Window(4, 85, 140, 59)
                    )
                ),
                Room(
                    "Office",
                    listOf(
                        Window(1, 85, 152, 53),
                        Window(2, 105, 152, 63),
                        Window(3, 115, 152, 129)
                    )
                ),
                Room(
                    "Hotel",
                    listOf(
                        Window(1, 1, 136, 93),
                        Window(2, -19, 136, 29),
                        Window(3, 51, 138, -7),
                        Window(4, 53, 138, 7),
                        Window(5, -7, 152, -43),
                        Window(6, 51, 152, -11)
                    )
                ),
                Room(
                    "Apartments",
                    listOf(
                        Window(1, 39, 152, 19),
                        Window(2, -31, 152, 31),
                        Window(3, -27, 152, 103),
                        Window(4, -9, 152, 125)
                    )
                ),
                Room(
                    "Power Station",
                    listOf(
                        Window(1, 7, 166, 125),
                        Window(2, -5, 166, 65),
                        Window(3, -11, 136, 133)
                    )
                ),
                Room(
                    "Rooftop",
                    listOf(
                        Window(1, -31, 166, 129),
                        Window(2, -27, 166, 61),
                        Window(3, -99, 166, 77),
                        Window(4, -75, 166, 51)
                    )
                ),
                Room(
                    "Gallery",
                    listOf(
                        Window(1, 45, 152, 155),
                        Window(2, 61, 152, 109),
                        Window(3, 31, 152, 131)
                    )
                ),
                Room(
                    "Garden",
                    listOf(
                        Window(1, 1, 136, -33),
                        Window(2, 49, 136, -67),
                        Window(3, 69, 136, -33)
                    )
                )
            )
        }

        val badBlood by lazy {
            listOf(
                Room(
                    "Courtyard",
                    listOf(
                        Window(1, 49, 138, -37),
                        Window(2, 61, 138, 21),
                        Window(3, 39, 138, 41),
                        Window(4, 25, 138, -35)
                    )
                ),
                Room(
                    "Mansion",
                    listOf(
                        Window(1, 1, 148, -35),
                        Window(2, 1, 148, 37),
                        Window(3, -25, 146, 57)
                    )
                ),
                Room(
                    "Library",
                    listOf(
                        Window(1, 3, 148, -89),
                        Window(2, -41, 148, -59),
                        Window(3, -81, 148, -61),
                        Window(4, -79, 148, -115),
                        Window(5, -109, 148, -93),
                        Window(6, -107, 148, -67)
                    )
                ),
                Room(
                    "Dungeon",
                    listOf(
                        Window(1, -57, 136, -69),
                        Window(2, -73, 136, -23),
                        Window(3, -19, 136, -37),
                        Window(4, -19, 136, -45),
                        Window(5, -21, 136, -99)
                    )
                ),
                Room(
                    "Crypts",
                    listOf(
                        Window(1, -7, 136, -5),
                        Window(2, -31, 136, 1),
                        Window(3, -57, 136, 41)
                    )
                ),
                Room(
                    "Graveyard",
                    listOf(
                        Window(1, -13, 136, 67),
                        Window(2, -71, 136, 63),
                        Window(3, -33, 136, 101)
                    )
                ),
                Room(
                    "Balcony",
                    listOf(
                        Window(1, -83, 136, 55),
                        Window(2, -107, 144, 25),
                        Window(3, -113, 148, 5),
                        Window(4, -65, 148, -37)
                    )
                ),
                Room(
                    "Great Hall",
                    listOf(
                        Window(1, -39, 148, -27),
                        Window(2, -63, 152, 31),
                        Window(3, -55, 148, 31)
                    )
                )
            )
        }

        val alienArcadium by lazy {
            listOf(
                Room(
                    "Park Entrance",
                    listOf(
                        Window(1, 13, 144, 63),
                        Window(2, -21, 144, -11),
                        Window(3, -43, 144, 21),
                        Window(4, -45, 144, 31),
                        Window(5, 45, 144, 27)
                    )
                ),
                Room(
                    "Roller Coaster",
                    listOf(
                        Window(1, -57, 144, 55),
                        Window(2, -25, 144, 79)
                    )
                ),
                Room(
                    "Ferris Wheel",
                    listOf(
                        Window(1, 35, 144, 89),
                        Window(2, 55, 144, 63)
                    )
                ),
                Room(
                    "Bumper Cars",
                    listOf(
                        Window(1, 67, 146, -3),
                        Window(2, 45, 146, -27)
                    )
                )
            )
        }
    }
}
