package com.gitlab.candicey.zombies.data

import com.gitlab.candicey.zombies.enum.RotationDirection

data class Window(
    val alias: Int,
    var x: Int,
    var y: Int,
    var z: Int,
) {
    var isActive: Boolean = false

    fun mirror() {
        x = -x
        z = -z
    }

    fun rotate(direction: RotationDirection) {
        when (direction) {
            RotationDirection.CLOCKWISE -> {
                val temp = x
                x = z
                z = -temp
            }
            RotationDirection.COUNTER_CLOCKWISE -> {
                val temp = x
                x = -z
                z = temp
            }
        }
    }

    fun rotate(rotationCount: Int) {
        val rotation = rotationCount % 4

        when (rotation) {
            1, -3 -> rotate(RotationDirection.CLOCKWISE)
            2, -2 -> mirror()
            3, -1 -> rotate(RotationDirection.COUNTER_CLOCKWISE)
        }
    }

    fun mirrorX() {
        x = -x
    }

    fun mirrorZ() {
        z = -z
    }
}
