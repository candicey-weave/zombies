package com.gitlab.candicey.zombies.enum

enum class RotationDirection {
    CLOCKWISE,
    COUNTER_CLOCKWISE,
}