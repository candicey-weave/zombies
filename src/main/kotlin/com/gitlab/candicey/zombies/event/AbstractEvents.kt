package com.gitlab.candicey.zombies.event

import com.gitlab.candicey.zombies.data.PowerUp
import net.minecraft.entity.EntityLivingBase
import net.weavemc.loader.api.event.ChatReceivedEvent
import net.weavemc.loader.api.event.Event

abstract class AbstractChatReceivedEvent(val chatReceivedEvent: ChatReceivedEvent) : Event()

abstract class AbstractPowerUpEvent(val entity: EntityLivingBase, val powerUp: PowerUp) : Event()