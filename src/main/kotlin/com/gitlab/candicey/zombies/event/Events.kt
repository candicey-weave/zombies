package com.gitlab.candicey.zombies.event

import net.minecraft.entity.EntityLivingBase
import net.weavemc.loader.api.event.CancellableEvent
import net.weavemc.loader.api.event.Event

data class TitleEvent(val title: String, val subtitle: String) : Event()

sealed class LivingUpdateEvent(val entity: EntityLivingBase) : Event() {
    class Pre(entity: EntityLivingBase) : LivingUpdateEvent(entity)
    class Post(entity: EntityLivingBase) : LivingUpdateEvent(entity)
}

sealed class RenderEntityModelEvent(val entity: EntityLivingBase) : CancellableEvent() {
    class Pre(entity: EntityLivingBase) : RenderEntityModelEvent(entity)
    class Post(entity: EntityLivingBase) : RenderEntityModelEvent(entity)
}