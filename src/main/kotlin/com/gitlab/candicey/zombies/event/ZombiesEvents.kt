package com.gitlab.candicey.zombies.event

import com.gitlab.candicey.zombies.data.PowerUp
import net.minecraft.entity.EntityLivingBase
import net.weavemc.loader.api.event.ChatReceivedEvent
import net.weavemc.loader.api.event.Event

object GameStartEvent : Event()

class GameOverEvent(val finalRound: Int) : Event()

class RoundNewEvent(val round: Int) : Event()

class TargetHitEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val player: String
) : AbstractChatReceivedEvent(chatReceivedEvent)

class LuckyChestEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val player: String,
    val item: String
) : AbstractChatReceivedEvent(chatReceivedEvent)

class GoldReceivedEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val gold: Int
) : AbstractChatReceivedEvent(chatReceivedEvent)

class ItemPurchasedEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val item: String,
) : AbstractChatReceivedEvent(chatReceivedEvent)

class PowerUpPickupEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val player: String,
    val powerUp: PowerUp,
    val duration: Int
) : AbstractChatReceivedEvent(chatReceivedEvent)

class PowerUpSpawnEvent(
    entity: EntityLivingBase,
    powerUp: PowerUp
) : AbstractPowerUpEvent(entity, powerUp)

class PowerUpDespawnEvent(
    entity: EntityLivingBase,
    powerUp: PowerUp
) : AbstractPowerUpEvent(entity, powerUp)

sealed class WindowRepairEvent(
    chatReceivedEvent: ChatReceivedEvent
) : AbstractChatReceivedEvent(chatReceivedEvent) {
    class Start(chatReceivedEvent: ChatReceivedEvent) : WindowRepairEvent(chatReceivedEvent)
    class Stop(chatReceivedEvent: ChatReceivedEvent) : WindowRepairEvent(chatReceivedEvent)
    class Finish(chatReceivedEvent: ChatReceivedEvent) : WindowRepairEvent(chatReceivedEvent)
    class EnemyNearby(chatReceivedEvent: ChatReceivedEvent) : WindowRepairEvent(chatReceivedEvent)
}

class AreaOpenedEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val area: String,
    val player: String
) : AbstractChatReceivedEvent(chatReceivedEvent)

sealed class PlayerConnectionStatusEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val player: String
) : AbstractChatReceivedEvent(chatReceivedEvent) {
    class Left(chatReceivedEvent: ChatReceivedEvent, player: String) : PlayerConnectionStatusEvent(chatReceivedEvent, player)
    class Rejoined(chatReceivedEvent: ChatReceivedEvent, player: String) : PlayerConnectionStatusEvent(chatReceivedEvent, player)
}

class PlayerKnockedDownEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val player: String,
    val killer: String,
    val location: String,
    val timeLeft: Int,
    val self: Boolean
) : AbstractChatReceivedEvent(chatReceivedEvent)

class PlayerRevivedEvent(
    chatReceivedEvent: ChatReceivedEvent,
    val reviver: String,
    val revivedPlayer: String
) : AbstractChatReceivedEvent(chatReceivedEvent)
