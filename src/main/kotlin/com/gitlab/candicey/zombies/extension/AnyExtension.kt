package com.gitlab.candicey.zombies.extension

import java.lang.reflect.Modifier
import java.util.*

fun Any.reflectionToString(): String {
    val s = LinkedList<String>()
    var clazz: Class<in Any>? = javaClass

    while (clazz != null) {
        for (prop in clazz.declaredFields.filterNot { Modifier.isStatic(it.modifiers) }) {
            prop.isAccessible = true
            s += "${prop.name}=" + prop.get(this)?.toString()?.trim()
        }

        clazz = clazz.superclass
    }

    return "${javaClass.name}=[${s.joinToString(", ")}]"
}