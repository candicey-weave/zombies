package com.gitlab.candicey.zombies.extension

val Class<*>.strippedZombiesPackageName: String
    get() = this.name.removePrefix("com.gitlab.candicey.zombies.")