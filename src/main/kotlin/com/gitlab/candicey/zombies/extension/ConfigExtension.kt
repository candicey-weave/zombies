package com.gitlab.candicey.zombies.extension

import cc.polyfrost.oneconfig.config.Config
import cc.polyfrost.oneconfig.config.core.OneKeyBind
import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zombies.refreshOneConfigGui
import com.gitlab.candicey.zombies.sendNotification
import java.util.function.Supplier
import kotlin.reflect.KProperty0

fun Config.reopenGui() {
    refreshOneConfigGui()
    openGui()
}

fun Config.reopenGuiOperation(operation: () -> Unit) {
    operation()
    reopenGui()
}

fun Config.registerKeyBind(oneKeyBind: OneKeyBind, runnable: Runnable) =
    reflectMethod("registerKeyBind", OneKeyBind::class.java, Runnable::class.java)
        .invoke(this, oneKeyBind, runnable)

fun Config.registerToggleKeyBind(oneKeyBind: OneKeyBind) =
    registerKeyBind(oneKeyBind) {
        enabled = !enabled
        sendModuleToggleStateNotification()
        save()
        refreshOneConfigGui()
    }

fun Config.sendModuleToggleStateNotification() =
    sendNotification(
        mod.name,
        if (enabled) "(+) Enabled" else "(-) Disabled",
        2500f
    )

fun Config.addDependency(option: KProperty0<*>, dependentOption: KProperty0<*>) =
    reflectMethod("addDependency", String::class.java, String::class.java)
        .invoke(this, option.name, dependentOption.name)

fun Config.addDependencyAll(vararg args: KProperty0<*>) {
    require(args.size > 1) { "There must be at least 2 arguments, the option and the dependent option." }

    // the last argument is the dependent option
    for (i in 0 until args.size - 1) {
        addDependency(args[i], args[args.size - 1])
    }
}

fun Config.addDependency(conditionName: String, option: KProperty0<*>, supplier: Supplier<Boolean>) =
    reflectMethod("addDependency", String::class.java, String::class.java, Supplier::class.java)
        .invoke(this, option.name, conditionName, supplier)

fun Config.addDependencyAll(conditionName: String, vararg args: KProperty0<*>, supplier: Supplier<Boolean>) {
    for (i in 0 until args.size - 1) {
        addDependency(conditionName, args[i], supplier)
    }
}

fun Config.addListener(option: KProperty0<*>, listener: Runnable) =
    reflectMethod("addListener", String::class.java, Runnable::class.java)
        .invoke(this, option.name, listener)