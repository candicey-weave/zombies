package com.gitlab.candicey.zombies.extension

import com.gitlab.candicey.zenithcore.enum.CornerPosition

fun CornerPosition.shouldAlignRight() =
    this == CornerPosition.TOP_RIGHT || this == CornerPosition.BOTTOM_RIGHT

fun CornerPosition.shouldAlignBottom() =
    this == CornerPosition.BOTTOM_LEFT || this == CornerPosition.BOTTOM_RIGHT