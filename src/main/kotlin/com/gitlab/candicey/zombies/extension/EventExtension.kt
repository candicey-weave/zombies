package com.gitlab.candicey.zombies.extension

import net.weavemc.loader.api.event.Event
import net.weavemc.loader.api.event.EventBus

fun Event.publish() = EventBus.callEvent(this)

operator fun Event.unaryPlus() = publish()

val Event.strippedZombiesPackageName: String
    get() = this::class.java.strippedZombiesPackageName