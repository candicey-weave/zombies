package com.gitlab.candicey.zombies.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiIngame

var GuiIngame.displayedTitle: String? by ShadowField()

var GuiIngame.displayedSubTitle: String? by ShadowField()