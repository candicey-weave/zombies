package com.gitlab.candicey.zombies.extension

fun ShortArray.indexOfFirstOrNull(predicate: (Short) -> Boolean): Int? {
    for (i in indices) {
        if (predicate(this[i])) {
            return i
        }
    }

    return null
}