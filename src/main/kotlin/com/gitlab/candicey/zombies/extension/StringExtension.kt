package com.gitlab.candicey.zombies.extension

import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.sendChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zenithcore.util.GREY
import com.gitlab.candicey.zenithcore.util.LIGHT_PURPLE
import com.gitlab.candicey.zenithcore.util.YELLOW
import com.gitlab.candicey.zombies.data.ChatOutput

private fun String.addPrefix(prefix: String): String = "$GREY[$LIGHT_PURPLE$prefix$GREY]$YELLOW $this"

fun String.addZPrefix(): String = addPrefix("Zombies")

fun String.addZSVPrefix(): String = addPrefix("ZSV")

fun String.addZCPrefix(): String = addPrefix("ZC")

fun String.addZHFPrefix(): String = addPrefix("ZHF")

fun String.addZDCPrefix(): String = addPrefix("ZDC")

fun String.addNEZPrefix(): String = addPrefix("NEZ")

fun String.addSLAPrefix(): String = addPrefix("SLA")

/**
 * Sends the message to the target determined by the [ChatOutput] enum.
 */
fun String.message(chatOutput: ChatOutput, prefix: (String.() -> String)?) {
    when (chatOutput) {
        ChatOutput.CHAT -> "/ac $this".toChatComponent().sendChatMessage()
        ChatOutput.PARTY -> "/pc $this".toChatComponent().sendChatMessage()
        ChatOutput.SELF -> (prefix?.invoke(this) ?: this).toChatComponent().addChatMessage()
        ChatOutput.OFF -> {}
    }
}