package com.gitlab.candicey.zombies.extension

import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.gui.OneConfigGui
import cc.polyfrost.oneconfig.renderer.font.Font
import cc.polyfrost.oneconfig.renderer.font.Fonts
import cc.polyfrost.oneconfig.utils.dsl.VG
import cc.polyfrost.oneconfig.utils.dsl.drawText
import cc.polyfrost.oneconfig.utils.dsl.getTextWidth
import com.gitlab.candicey.zenithcore.displayHeight
import com.gitlab.candicey.zenithcore.displayWidth
import com.gitlab.candicey.zenithcore.enum.CornerPosition
import com.gitlab.candicey.zombies.brightness
import com.gitlab.candicey.zombies.util.CornerPositionUtil
import com.gitlab.candicey.zombies.whiteColour

fun VG.drawTextRightAligned(text: String, x: Float, y: Float, color: Int, size: Float, font: Font = Fonts.REGULAR) {
    val width = instance.getTextWidth(text, size, font)
    instance.drawText(text, x - width, y, color, size, font)
}

fun VG.drawTextRightAlignedWithShadow(text: String, x: Float, y: Float, colour: OneColor = whiteColour, size: Float, font: Font = Fonts.REGULAR, shadowOffsetX: Float, shadowOffsetY: Float, shadowBrightness: Float = 0.2f) {
    drawTextRightAligned(text, x + shadowOffsetX, y + shadowOffsetY, brightness(colour, shadowBrightness).rgb, size, font)
    drawTextRightAligned(text, x, y, colour.rgb, size, font)
}

fun VG.drawTextRightAlignedWithShadow(text: String, x: Float, y: Float, colour: OneColor = whiteColour, size: Float, font: Font = Fonts.REGULAR, shadowOffset: Float, shadowBrightness: Float = 0.2f) =
    drawTextRightAlignedWithShadow(text, x, y, colour, size, font, shadowOffset, shadowOffset, shadowBrightness)

fun VG.drawTextWithShadow(text: String, x: Float, y: Float, colour: OneColor = whiteColour, size: Number, font: Font = Fonts.REGULAR, shadowOffsetX: Float, shadowOffsetY: Float, shadowBrightness: Float = 0.2f) {
    drawText(text, x + shadowOffsetX, y + shadowOffsetY, brightness(colour, shadowBrightness).rgb, size, font)
    drawText(text, x, y, colour.rgb, size, font)
}

fun VG.drawTextWithShadow(text: String, x: Float, y: Float, colour: OneColor = whiteColour, size: Number, font: Font = Fonts.REGULAR, shadowOffset: Float, shadowBrightness: Float = 0.2f) =
    drawTextWithShadow(text, x, y, colour, size, font, shadowOffset, shadowOffset, shadowBrightness)

fun VG.drawText(text: String, x: Float, y: Float, colour: OneColor = whiteColour, size: Number, font: Font = Fonts.REGULAR, alignRight: Boolean = false, shadow: Boolean = false, shadowOffset: Float = size.toFloat() / 15, shadowBrightness: Float = 0.2f) =
    if (alignRight) {
        if (shadow) drawTextRightAlignedWithShadow(text, x, y, colour, size.toFloat(), font, shadowOffset, shadowBrightness)
        else drawTextRightAligned(text, x, y, colour.rgb, size.toFloat(), font)
    } else {
        if (shadow) drawTextWithShadow(text, x, y, colour, size, font, shadowOffset, shadowBrightness)
        else drawText(text, x, y, colour.rgb, size, font)
    }

fun VG.drawText(text: String, x: Float = 0f, y: Float = 0f, autoOffsetY: Boolean = true, colour: OneColor = whiteColour, size: Float, font: Font = Fonts.REGULAR, corner: CornerPosition = CornerPosition.TOP_LEFT, marginX: Float = 0f, marginY: Float = 0f, shadow: Boolean = false, shadowOffset: Float = size / 15, shadowBrightness: Float = 0.2f): Pair<Float, Float> {
    val scaledMarginX = marginX * OneConfigGui.getScaleFactor() / 1.25f
    val scaledMarginY = marginY * OneConfigGui.getScaleFactor() / 1.25f

    val calculatedY = if (autoOffsetY) y + size / 2 else y

    when (corner) {
        CornerPosition.TOP_LEFT -> drawText(text, x + scaledMarginX, calculatedY + scaledMarginY, colour, size, font, false, shadow, shadowOffset, shadowBrightness)
        CornerPosition.TOP_RIGHT -> drawText(text, displayWidth - x - scaledMarginX, calculatedY + scaledMarginY, colour, size, font, true, shadow, shadowOffset, shadowBrightness)
        CornerPosition.BOTTOM_LEFT -> drawText(text, x + scaledMarginX, displayHeight - calculatedY - scaledMarginY, colour, size, font, false, shadow, shadowOffset, shadowBrightness)
        CornerPosition.BOTTOM_RIGHT -> drawText(text, displayWidth - x - scaledMarginX, displayHeight - calculatedY - scaledMarginY, colour, size, font, true, shadow, shadowOffset, shadowBrightness)
    }

    return scaledMarginX to scaledMarginY
}

fun VG.drawLines(lines: List<String>, x: Float = 0f, y: Float = 0f, colour: OneColor = whiteColour, size: Float, font: Font = Fonts.REGULAR, corner: CornerPosition = CornerPosition.TOP_LEFT, marginX: Float = 0f, marginY: Float = 0f, shadow: Boolean = false, shadowOffset: Float = size / 15, shadowBrightness: Float = 0.2f) {
    for ((yIndex, line) in lines.withIndex()) {
        drawText(
            text = line,
            x = x,
            y = y + yIndex * (size / 0.875f),
            colour = colour,
            size = size,
            font = font,
            corner = corner,
            marginX = marginX,
            marginY = marginY,
            shadow = shadow,
            shadowOffset = shadowOffset,
            shadowBrightness = shadowBrightness,
        )
    }
}

fun VG.drawLines(lines: List<String>, x: Float = 0f, y: Float = 0f, colour: OneColor = whiteColour, size: Float, font: Font = Fonts.REGULAR, cornerIndex: Int = 0, marginX: Float = 0f, marginY: Float = 0f, shadow: Boolean = false, shadowOffset: Float = size / 15, shadowBrightness: Float = 0.2f) =
    drawLines(lines, x, y, colour, size, font, CornerPositionUtil.fromIndex(cornerIndex), marginX, marginY, shadow, shadowOffset, shadowBrightness)


fun VG.drawLines(lineProvider: (yIndex: Int) -> String?, x: Float = 0f, y: Float = 0f, colourProvider: (yIndex: Int) -> OneColor?, size: Float, font: Font = Fonts.REGULAR, corner: CornerPosition = CornerPosition.TOP_LEFT, marginX: Float = 0f, marginY: Float = 0f, shadowProvider: (yIndex: Int) -> Boolean = { false }, shadowOffsetProvider: (yIndex: Int) -> Float = { size / 15 }, shadowBrightnessProvider: (yIndex: Int) -> Float = { 0.2f }) {
    for (yIndex in 0 until Int.MAX_VALUE) {
        val line = lineProvider(yIndex) ?: break
        val colour = colourProvider(yIndex) ?: whiteColour
        val shadow = shadowProvider(yIndex)
        val shadowOffset = shadowOffsetProvider(yIndex)
        val shadowBrightness = shadowBrightnessProvider(yIndex)

        drawText(
            text = line,
            x = x,
            y = y + yIndex * (size / 0.875f),
            colour = colour,
            size = size,
            font = font,
            corner = corner,
            marginX = marginX,
            marginY = marginY,
            shadow = shadow,
            shadowOffset = shadowOffset,
            shadowBrightness = shadowBrightness,
        )
    }
}

fun VG.drawLines(lineProvider: (yIndex: Int) -> String?, x: Float = 0f, y: Float = 0f, colourProvider: (yIndex: Int) -> OneColor?, size: Float, font: Font = Fonts.REGULAR, cornerIndex: Int = 0, marginX: Float = 0f, marginY: Float = 0f, shadow: Boolean = false, shadowOffset: Float = size / 15, shadowBrightness: Float = 0.2f) =
    drawLines(lineProvider, x, y, colourProvider, size, font, CornerPositionUtil.fromIndex(cornerIndex), marginX, marginY, { shadow }, { shadowOffset }, { shadowBrightness })