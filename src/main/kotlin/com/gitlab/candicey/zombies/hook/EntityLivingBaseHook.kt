package com.gitlab.candicey.zombies.hook

import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zombies.event.LivingUpdateEvent
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

/**
 * @see [net.minecraft.entity.EntityLivingBase.onLivingUpdate]
 */
object EntityLivingBaseHook : Hook("net/minecraft/entity/EntityLivingBase") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val onLivingUpdate = node.methods.named("onLivingUpdate")
        val instructions = onLivingUpdate.instructions

        instructions.insert(generateAsm<LivingUpdateEvent.Pre>())
        instructions.insertBeforeReturn(generateAsm<LivingUpdateEvent.Post>())
    }

    private inline fun <reified T : LivingUpdateEvent> generateAsm() = asm {
        new(internalNameOf<T>())
        dup
        aload(0)
        invokespecial(internalNameOf<T>(), "<init>", "(Lnet/minecraft/entity/EntityLivingBase;)V")
        callEvent()
    }
}