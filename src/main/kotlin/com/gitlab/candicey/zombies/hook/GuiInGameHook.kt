package com.gitlab.candicey.zombies.hook

import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zombies.event.TitleEvent
import com.gitlab.candicey.zombies.extension.displayedSubTitle
import com.gitlab.candicey.zombies.extension.displayedTitle
import com.gitlab.candicey.zombies.extension.unaryPlus
import net.minecraft.client.gui.GuiIngame
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Opcodes.GETFIELD
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode
import org.objectweb.asm.tree.JumpInsnNode

/**
 * @see [net.minecraft.client.gui.GuiIngame.renderGameOverlay]
 */
object GuiInGameHook : Hook("net/minecraft/client/gui/GuiIngame") {
    private val ignoredTitle = listOf("§r", "")

    private var previousEvent: TitleEvent? = null

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val renderGameOverlay = node.methods.named("renderGameOverlay")
        val instructions = renderGameOverlay.instructions

        val recordPlayingUpFor = instructions.find { insnNode -> insnNode is FieldInsnNode && insnNode.opcode == GETFIELD && insnNode.owner == "net/minecraft/client/gui/GuiIngame" && insnNode.name == "recordPlayingUpFor" && insnNode.next.let { it is JumpInsnNode && it.opcode == Opcodes.IFLE } }!!
        val ifle = recordPlayingUpFor.next as JumpInsnNode

        instructions.insert(ifle.label, asm {
            aload(0)
            invokestatic(internalNameOf<GuiInGameHook>(), ::onRenderTitle.name, "(Lnet/minecraft/client/gui/GuiIngame;)V")
        })

        cfg.computeFrames()
    }

    @JvmStatic
    fun onRenderTitle(instance: GuiIngame) {
        val title = instance.displayedTitle ?: return
        val subtitle = instance.displayedSubTitle ?: return

        if (title in ignoredTitle && subtitle in ignoredTitle) {
            return
        }

        val titleEvent = TitleEvent(title, subtitle)
        if (titleEvent == previousEvent) {
            return
        }

        previousEvent = titleEvent
        +titleEvent
    }
}