package com.gitlab.candicey.zombies.hook

import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zombies.mod.hologramfixer.ZombiesHologramFixer
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

/**
 * @see [net.minecraft.network.NetworkManager.channelRead0]
 */
object NetworkManagerHook : Hook("net/minecraft/network/NetworkManager") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val channelRead0 = node.methods.named("channelRead0")
        val instructions = channelRead0.instructions

        instructions.insert(asm {
            aload(2)
            invokestatic(
                internalNameOf<ZombiesHologramFixer>(),
                ZombiesHologramFixer::onPacketReceive.name,
                "(Lnet/minecraft/network/Packet;)V",
            )
        })
    }
}