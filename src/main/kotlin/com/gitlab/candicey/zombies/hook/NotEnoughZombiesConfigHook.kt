package com.gitlab.candicey.zombies.hook

import cc.polyfrost.oneconfig.config.annotations.Checkbox
import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zombies.data.Constant.CATEGORY_DEBUG
import com.gitlab.candicey.zombies.event.*
import com.gitlab.candicey.zombies.extension.strippedZombiesPackageName
import net.weavemc.loader.api.Hook
import org.objectweb.asm.Opcodes.ACC_PUBLIC
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

/**
 * @see [com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig]
 */
object NotEnoughZombiesConfigHook : Hook("com/gitlab/candicey/zombies/config/NotEnoughZombiesConfig") {
    // mutable map of event class to field name
    val entries = mutableMapOf<Class<*>, String>()

    private val events = listOf(
        AbstractChatReceivedEvent::class.java,
        AbstractPowerUpEvent::class.java,
        TitleEvent::class.java,
        LivingUpdateEvent::class.java, LivingUpdateEvent.Pre::class.java, LivingUpdateEvent.Post::class.java,
        GameStartEvent::class.java,
        GameOverEvent::class.java,
        RoundNewEvent::class.java,
        TargetHitEvent::class.java,
        LuckyChestEvent::class.java,
        GoldReceivedEvent::class.java,
        ItemPurchasedEvent::class.java,
        PowerUpPickupEvent::class.java,
        PowerUpSpawnEvent::class.java,
        PowerUpDespawnEvent::class.java,
        WindowRepairEvent::class.java, WindowRepairEvent.Start::class.java, WindowRepairEvent.Stop::class.java, WindowRepairEvent.Finish::class.java, WindowRepairEvent.EnemyNearby::class.java,
        AreaOpenedEvent::class.java,
        PlayerConnectionStatusEvent::class.java, PlayerConnectionStatusEvent.Left::class.java, PlayerConnectionStatusEvent.Rejoined::class.java,
        PlayerKnockedDownEvent::class.java,
        PlayerRevivedEvent::class.java,
    )

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        for (event in events) {
            addDebugNotificationEventEntry(node, event)
        }
    }

    private fun addDebugNotificationEventEntry(classNode: ClassNode, eventClass: Class<*>) {
        val strippedName = eventClass.strippedZombiesPackageName.removePrefix("event.").replace('.', '$')
        val fieldName = "${strippedName}_EventEntry"
        val field = FieldNode(
            ACC_PUBLIC,
            fieldName,
            "Z",
            null,
            false
        ).apply {
            val checkBoxAnnotation = AnnotationNode("L${internalNameOf<Checkbox>()};")
                .apply {
                    values = listOf(
                        "name", strippedName,
                        "description", eventClass.name,
                        "category", CATEGORY_DEBUG,
                    )
                }
            val debugNotificationEventEntryAnnotation = AnnotationNode("L${internalNameOf<DebugNotificationEventEntry>()};")

            visibleAnnotations = listOf(
                checkBoxAnnotation,
                debugNotificationEventEntryAnnotation,
            )
        }

        classNode.addFieldSafe(field)
        entries[eventClass] = fieldName
    }

    annotation class DebugNotificationEventEntry
}