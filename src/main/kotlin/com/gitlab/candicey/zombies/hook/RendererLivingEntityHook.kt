package com.gitlab.candicey.zombies.hook

import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zombies.event.RenderEntityModelEvent
import net.weavemc.loader.api.Hook
import net.weavemc.loader.api.util.asm
import org.objectweb.asm.tree.ClassNode

/**
 * see [net.minecraft.client.renderer.entity.RendererLivingEntity.renderModel]
 */
object RendererLivingEntityHook : Hook("net/minecraft/client/renderer/entity/RendererLivingEntity") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val renderModel = node.methods.named("renderModel")
        val instructions = renderModel.instructions

        instructions.insert(generateAsm<RenderEntityModelEvent.Pre>())
        instructions.insertBeforeReturn(generateAsm<RenderEntityModelEvent.Post>())
    }

    private inline fun <reified T : Any> generateAsm() = asm {
        new(internalNameOf<T>())
        dup
        aload(1)
        invokespecial(internalNameOf<T>(), "<init>", "(Lnet/minecraft/entity/EntityLivingBase;)V")
        callEvent()
    }
}