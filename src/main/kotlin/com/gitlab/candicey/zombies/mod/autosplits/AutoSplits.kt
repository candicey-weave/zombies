package com.gitlab.candicey.zombies.mod.autosplits

import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.eventbus.Subscribe
import cc.polyfrost.oneconfig.utils.dsl.nanoVG
import com.gitlab.candicey.zombies.config.AutoSplitsConfig
import com.gitlab.candicey.zombies.extension.drawText
import com.gitlab.candicey.zombies.mod.autosplits.splitter.ISplitter
import com.gitlab.candicey.zombies.mod.autosplits.splitter.InternalSplitter
import com.gitlab.candicey.zombies.mod.autosplits.splitter.LiveSplitSocketSplitter
import com.gitlab.candicey.zombies.sendNotification
import com.gitlab.candicey.zombies.util.CornerPositionUtil
import com.gitlab.candicey.zombies.warn
import net.minecraft.network.play.server.S29PacketSoundEffect
import net.weavemc.loader.api.event.PacketEvent
import net.weavemc.loader.api.event.SubscribeEvent
import java.util.concurrent.Executors

object AutoSplits {
    val executor = Executors.newSingleThreadScheduledExecutor()

    var splitter: ISplitter? = null

    init {
        initialiseSplitter()
    }

    fun initialiseSplitter() = with(AutoSplitsConfig) {
        splitter = if (liveSplit) {
            LiveSplitSocketSplitter(address, checkAndGetPort() ?: return)
        } else {
            InternalSplitter()
        }
    }

    fun reloadSplitter() {
        (splitter as? InternalSplitter)?.cancel()

        initialiseSplitter()
    }

    @Subscribe
    fun onHudRenderEvent(event: HudRenderEvent) = with(AutoSplitsConfig) {
        if (!enabled) {
            return
        }

        val internalSplitter = splitter as? InternalSplitter ?: return

        nanoVG {
            val milliseconds = internalSplitter.milliseconds
            val minute = getMinute(milliseconds)
            val second = getSecond(milliseconds)
            val tenthSecond = getTenthSecond(milliseconds)

            val text = String.format("%d:%02d.%d", minute, second, tenthSecond)

            drawText(
                text = text,
                size = fontSize.toFloat(),
                colour = textColour,
                corner = CornerPositionUtil.fromIndex(position),
                marginX = textMarginX.toFloat(),
                marginY = textMarginY.toFloat(),
                shadow = textShadow,
            )
        }
    }

    @SubscribeEvent
    fun onPacketReceivedEvent(event: PacketEvent.Receive) {
        if (!AutoSplitsConfig.enabled) {
            return
        }

        val packet = event.packet as? S29PacketSoundEffect ?: return

        if (packet.soundName == "mob.wither.spawn" || packet.soundName == "mob.enderdragon.end") {
            splitter?.startOrSplit()?.exceptionally {
                warn("Failed to start/split:", it)

                sendNotification(
                    "Auto Splits",
                    "Failed to start/split"
                )

                return@exceptionally null
            }
        }
    }

    /**
     * 0 <= output < 60
     */
    fun getMinute(milliseconds: Long) = milliseconds / 60000

    /**
     * 0 <= output < 60
     */
    fun getSecond(milliseconds: Long) = (milliseconds / 1000) % 60

    /**
     * 0 <= output < 10
     */
    fun getTenthSecond(milliseconds: Long) = (milliseconds / 100) % 10
}