package com.gitlab.candicey.zombies.mod.autosplits.splitter

import java.util.concurrent.CompletableFuture

interface ISplitter {
    fun startOrSplit(): CompletableFuture<Unit>
}