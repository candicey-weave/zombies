package com.gitlab.candicey.zombies.mod.autosplits.splitter

import com.gitlab.candicey.zombies.mod.autosplits.AutoSplits.executor
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.ReentrantLock

class InternalSplitter : ISplitter {
    private val lock = ReentrantLock()

    var future: Future<*>? = null

    @Volatile
    @set:Synchronized
    var milliseconds: Long = 0

    override fun startOrSplit(): CompletableFuture<Unit> = lockOperation {
        if (future == null) {
            future = executor.scheduleAtFixedRate({
                lockOperation {
                    milliseconds += 10
                }
            }, 0, 10, TimeUnit.MILLISECONDS)
        } else {
            milliseconds = 0
        }

        CompletableFuture.completedFuture(null)
    }

    fun cancel() = lockOperation {
        future?.cancel(true)
        future = null
    }

    private fun <T> lockOperation(operation: () -> T): T =
        try {
            lock.lock()
            operation()
        } finally {
            lock.unlock()
        }
}