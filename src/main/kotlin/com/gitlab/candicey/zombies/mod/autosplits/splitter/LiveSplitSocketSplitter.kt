package com.gitlab.candicey.zombies.mod.autosplits.splitter

import com.gitlab.candicey.zombies.mod.autosplits.AutoSplits.executor
import java.io.PrintWriter
import java.net.Socket
import java.util.concurrent.CompletableFuture

class LiveSplitSocketSplitter(val host: String, val port: Int) : ISplitter {
    override fun startOrSplit(): CompletableFuture<Unit> = sendSocketMessage("startorsplit")

    private fun sendSocketMessage(message: String): CompletableFuture<Unit> {
        val completableFuture = CompletableFuture<Unit>()

        executor.execute {
            try {
                val socket = Socket(host, port)
                val writer = PrintWriter(socket.getOutputStream(), true)

                writer.println(message)

                writer.close()
                socket.close()

                completableFuture.complete(null)
            } catch (e: Exception) {
                completableFuture.completeExceptionally(e)
            }
        }

        return completableFuture
    }
}