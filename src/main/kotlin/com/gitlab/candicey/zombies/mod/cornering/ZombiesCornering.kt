package com.gitlab.candicey.zombies.mod.cornering

import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zombies.config.ZombiesCorneringConfig
import net.minecraft.entity.player.EntityPlayer
import net.weavemc.loader.api.event.RenderLivingEvent
import net.weavemc.loader.api.event.SubscribeEvent

object ZombiesCornering {
    @SubscribeEvent
    fun onRenderLivingEvent(event: RenderLivingEvent.Pre) {
        if (with(ZombiesCorneringConfig) { !enabled }) {
            return
        }

        if (event.entity !is EntityPlayer) {
            return
        }

        if (ZombiesCorneringConfig.showSelf && event.entity == mc.thePlayer) {
            return
        }

        if (event.entity.getDistanceToEntity(mc.thePlayer) < ZombiesCorneringConfig.distance) {
            event.cancelled = true
        }
    }
}