package com.gitlab.candicey.zombies.mod.damagepersecondcounter

import net.minecraft.entity.EntityLiving

data class HealthData(
    val entity: EntityLiving,
) {
    var health: Float = entity.health

    fun calculateDamage(): Float {
        val newHealth = entity.health
        val damage = health - newHealth
        health = newHealth
        return damage.coerceAtLeast(0f)
    }
}
