package com.gitlab.candicey.zombies.mod.damagepersecondcounter

import com.gitlab.candicey.zenithcore.event.EntityJoinWorldEvent
import com.gitlab.candicey.zenithcore.event.PlayerTickEvent
import com.gitlab.candicey.zenithcore.util.Cache
import com.gitlab.candicey.zombies.config.ZombiesDamagePerSecondCounterConfig
import net.minecraft.entity.EntityLiving
import net.weavemc.loader.api.event.SubscribeEvent
import java.util.concurrent.ConcurrentLinkedQueue


object ZombiesDamagePerSecondCounter {
    private val timeQueue = ConcurrentLinkedQueue<Long>()
    private val damageQueue = ConcurrentLinkedQueue<Float>()

    private val healthDataList = Cache<Int, HealthData>(
        cacheTime = 10 * 60 * 1000,
        maxCacheSize = 750
    )

    private var tick = 0

    @SubscribeEvent
    fun onTickEvent(event: PlayerTickEvent.Post) {
        if (checkUpdateTick()) {
            synchronized(healthDataList) {
                healthDataList.getAll().values.forEach { healthData ->
                    val damage = healthData.calculateDamage()
                    if (damage > 0) {
                        add(damage)
                    }

                    if (healthData.entity.health == 0f) {
                        healthDataList.remove(healthData.entity.entityId)
                    }
                }
            }
        }
    }

    @SubscribeEvent
    fun onEntityJoinWorldEvent(event: EntityJoinWorldEvent) {
        val entity = event.entity
        if (entity !is EntityLiving) {
            return
        }

        synchronized(healthDataList) {
            healthDataList.getOrPut(entity.entityId) { HealthData(entity) }
        }
    }

    private fun checkUpdateTick(): Boolean {
        val interval = ZombiesDamagePerSecondCounterConfig.updateInterval
        return if (interval == 1) {
            true
        } else if (++tick >= interval) {
            tick = 0
            true
        } else {
            false
        }
    }

    private fun add(enqueue: Float) {
        timeQueue.add(System.currentTimeMillis() + 1000L)
        damageQueue.add(enqueue)
    }

    fun count(): Float {
        val time = System.currentTimeMillis()
        while (!timeQueue.isEmpty() && timeQueue.peek() < time) {
            timeQueue.poll()
            damageQueue.poll()
        }
        return damageQueue.sum()
    }
}