package com.gitlab.candicey.zombies.mod.hologramfixer

import com.gitlab.candicey.zenithcore.extension.channel
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zenithcore.util.runDelayedAsync
import com.gitlab.candicey.zombies.config.ZombiesHologramFixerConfig
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelOutboundHandlerAdapter
import io.netty.channel.ChannelPromise
import net.minecraft.entity.item.EntityArmorStand
import net.minecraft.item.Item
import net.minecraft.network.Packet
import net.minecraft.network.login.server.S03PacketEnableCompression
import net.minecraft.network.play.client.C02PacketUseEntity
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement

object ZombiesHologramFixer {
    @JvmStatic
    fun onPacketReceive(packet: Packet<*>) {
        if (packet !is S03PacketEnableCompression) {
            return
        }

        runDelayedAsync(3500L) {
            mc.netHandler?.networkManager?.channel?.pipeline()?.addLast(object : ChannelOutboundHandlerAdapter() {
                override fun write(ctx: ChannelHandlerContext?, msg: Any?, promise: ChannelPromise?) {
                    try {
                        if (with(ZombiesHologramFixerConfig) { !enabled }) {
                            super.write(ctx, msg, promise)
                            return
                        }

                        if (msg is C02PacketUseEntity) {
                            if (msg.action == C02PacketUseEntity.Action.ATTACK) {
                                super.write(ctx, msg, promise)
                                return
                            }

                            val clicked = msg.getEntityFromWorld(mc.thePlayer.worldObj)
                            if (clicked !is EntityArmorStand) {
                                super.write(ctx, msg, promise)
                                return
                            }

                            val item = mc.thePlayer.inventory.getCurrentItem()
                            if (item != null && item.item === Item.getItemById(267)) {
                                super.write(ctx, msg, promise)
                                return
                            }

                            super.write(ctx, C08PacketPlayerBlockPlacement(item), promise)
                        } else {
                            super.write(ctx, msg, promise)
                        }
                    } catch (e: Throwable) {
                        e.printStackTrace()
                        super.write(ctx, msg, promise)
                    }
                }
            })
        }
    }
}