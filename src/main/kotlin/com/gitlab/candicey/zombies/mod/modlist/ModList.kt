package com.gitlab.candicey.zombies.mod.modlist

import cc.polyfrost.oneconfig.config.core.OneColor
import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.eventbus.Subscribe
import cc.polyfrost.oneconfig.renderer.font.Fonts
import cc.polyfrost.oneconfig.utils.dsl.getTextWidth
import cc.polyfrost.oneconfig.utils.dsl.nanoVG
import com.gitlab.candicey.zombies.config.ModListConfig
import com.gitlab.candicey.zombies.config.ZombiesCorneringConfig
import com.gitlab.candicey.zombies.config.ZombiesHologramFixerConfig
import com.gitlab.candicey.zombies.config.ZombiesStrategyViewerConfig
import com.gitlab.candicey.zombies.extension.drawLines
import com.gitlab.candicey.zombies.extension.shouldAlignBottom
import com.gitlab.candicey.zombies.extension.shouldAlignRight
import com.gitlab.candicey.zombies.util.CornerPositionUtil

object ModList {
    @Subscribe
    fun onHudRenderEvent(event: HudRenderEvent) = with(ModListConfig) {
        if (!enabled) {
            return
        }

        nanoVG {
            val fontSize = fontSize.toFloat()
            val font = Fonts.MEDIUM
            val distanceOffset = fontSize * 5 / 14
            val position = CornerPositionUtil.fromIndex(position)

            val modules = getModules()
            fun getModule(yIndex: Int): Pair<String, State>? =
                if (yIndex == modules.size) {
                    null
                } else {
                    modules[if (position.shouldAlignBottom()) modules.size - yIndex - 1 else yIndex]
                }
            val getModuleText = { yIndex: Int -> getModule(yIndex)?.first }
            val getModuleStateText = { yIndex: Int -> getModule(yIndex)?.second?.text }
            val getModuleStateColour = { yIndex: Int ->
                when (getModule(yIndex)?.second) {
                    State.ON -> enabledTextColour
                    State.OFF -> disabledTextColour
                    null -> null
                }
            }

            val posXText = if (position.shouldAlignRight()) getTextWidth(State.OFF.text, fontSize, font) + distanceOffset else 0f
            val posXState = if (position.shouldAlignRight()) 0f else getTextWidth(modules.maxOf { it.first }, fontSize, font) + distanceOffset

            drawLines(
                lineProvider = getModuleText,
                x = posXText,
                colourProvider = { textColour },
                size = fontSize,
                font = font,
                corner = position,
                marginX = textMarginX.toFloat(),
                marginY = textMarginY.toFloat(),
                shadowProvider = { textShadow },
            )
            drawLines(
                lineProvider = getModuleStateText,
                x = posXState,
                colourProvider = getModuleStateColour,
                size = fontSize,
                font = font,
                corner = position,
                marginX = textMarginX.toFloat(),
                marginY = textMarginY.toFloat(),
                shadowProvider = { textShadow },
            )
        }
    }

    private fun getModules() = listOf(
        "Zombies Strategy Viewer" to State.fromBoolean(ZombiesStrategyViewerConfig.enabled),
        "Zombies Cornering" to State.fromBoolean(ZombiesCorneringConfig.enabled),
        "Zombies Hologram Fixer" to State.fromBoolean(ZombiesHologramFixerConfig.enabled),
    )

    enum class State(val text: String, val colour: OneColor) {
        ON("ON ", OneColor(0xFF00FF00.toInt())),
        OFF("OFF", OneColor(0xFFFF0000.toInt()));

        companion object {
            fun fromBoolean(boolean: Boolean) = if (boolean) ON else OFF
        }
    }
}