package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.event.*
import net.weavemc.loader.api.event.SubscribeEvent

object HideMessages {
    @SubscribeEvent
    fun onGoldReceive(event: GoldReceivedEvent) {
        if (!NotEnoughZombiesConfig.hideGoldMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onKnockdownEvent(event: PlayerKnockedDownEvent) {
        if (!NotEnoughZombiesConfig.hideKnockdownMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onReviveEvent(event: PlayerRevivedEvent) {
        if (!NotEnoughZombiesConfig.hideReviveMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onGoldReceive(event: WindowRepairEvent) {
        if (!NotEnoughZombiesConfig.hideWindowRepairMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onHitTargetEvent(event: TargetHitEvent) {
        if (!NotEnoughZombiesConfig.hideHitTargetMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onLuckyChestEvent(event: LuckyChestEvent) {
        if (!NotEnoughZombiesConfig.hideLuckyChestMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onOpenAreaEvent(event: AreaOpenedEvent) {
        if (!NotEnoughZombiesConfig.hideOpenAreaMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onPlayerConnectionStatusEvent(event: PlayerConnectionStatusEvent) {
        if (!NotEnoughZombiesConfig.hidePlayerConnectionStatusMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }

    @SubscribeEvent
    fun onPowerUpPickupEvent(event: PowerUpPickupEvent) {
        if (!NotEnoughZombiesConfig.hidePowerUpPickupMessages) {
            return
        }

        event.chatReceivedEvent.cancelled = true
    }
}