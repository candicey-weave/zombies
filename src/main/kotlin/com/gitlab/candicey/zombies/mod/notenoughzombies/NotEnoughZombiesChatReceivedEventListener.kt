package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zombies.data.PowerUp
import com.gitlab.candicey.zombies.event.*
import com.gitlab.candicey.zombies.extension.unaryPlus
import com.gitlab.candicey.zombies.util.ZombiesUtil
import com.gitlab.candicey.zombies.warn
import net.weavemc.loader.api.event.ChatReceivedEvent
import net.weavemc.loader.api.event.SubscribeEvent

object NotEnoughZombiesChatReceivedEventListener {
    private val chatHandlers = listOf(
        PlayerKnockedDownChatHandler,
        PlayerRevivedChatHandler,
        PowerUpChatHandler,
        LuckyChestFoundChatHandler,
        LuckyChestSelfFoundChatHandler,
        PlayerLeftChatHandler,
        PlayerRejoinedChatHandler,
        AreaOpenedChatHandler,
        ItemPurchasedChatHandler,
        HitTargetChatHandler,
        GoldReceivedChatHandler,
        WindowRepairStartChatHandler,
        WindowRepairStopChatHandler1,
        WindowRepairStopChatHandler2,
        WindowRepairStopChatHandler3,
        WindowRepairEnemyNearbyChatHandler,
        WindowRepairFinishChatHandler,
    )

    @SubscribeEvent
    fun onChatReceivedEvent(event: ChatReceivedEvent) {
        if (!ZombiesUtil.isEnabled()) {
            return
        }

        chatHandlers
            .filter { it.chatMessageRegex.matches(event.message.unformattedText) }
            .forEach { it.handle(event) }
    }

    private abstract class AbstractChatHandler(val chatMessageRegex: Regex) {
        abstract fun handle(event: ChatReceivedEvent)

        open fun ChatReceivedEvent.matchResult() = chatMessageRegex.find(message.unformattedText)
    }

    private object PlayerKnockedDownChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) was knocked down by (.+) in (.+)! You have (\\d+)s to (get revived|revive them)!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]
            val killer = matchResult.groupValues[2]
            val location = matchResult.groupValues[3]
            val timeLeft = matchResult.groupValues[4].toInt()
            val self = matchResult.groupValues[5] == "get revived"

            +PlayerKnockedDownEvent(event, player, killer, location, timeLeft, self)
        }
    }

    private object PlayerRevivedChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) revived ([a-zA-Z0-9_]{3,16})!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val reviver = matchResult.groupValues[1]
            val revivedPlayer = matchResult.groupValues[2]

            +PlayerRevivedEvent(event, reviver, revivedPlayer)
        }
    }

    private object PowerUpChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) activated (Shopping Spree|Double Gold|Max Ammo|Insta Kill|Carpenter|Bonus Gold)(?:| for (\\d+)s)!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]
            val powerUpName = matchResult.groupValues[2]
            val durationRaw = matchResult.groupValues.getOrNull(3)

            val powerUpOption = PowerUp.fromName(powerUpName)
            if (powerUpOption == null) {
                warn("Power up not found for player=$player, powerUp=$powerUpName, durationRaw=$durationRaw")
                return
            }
            val duration = durationRaw?.toIntOrNull() ?: -1


            +PowerUpPickupEvent(event, player, powerUpOption, duration)
        }
    }

    private object LuckyChestFoundChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) found (.+) in the Lucky Chest!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]
            val item = matchResult.groupValues[2]

            +LuckyChestEvent(event, player, item)
        }
    }

    private object LuckyChestSelfFoundChatHandler : AbstractChatHandler(
        Regex("You found (.+) in the Lucky Chest! You have 10s to claim it before it disappears!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = mc.thePlayer.name
            val item = matchResult.groupValues[1]

            +LuckyChestEvent(event, player, item)
        }
    }

    private object PlayerLeftChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) left the game\\.")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]

            +PlayerConnectionStatusEvent.Left(event, player)
        }
    }

    private object PlayerRejoinedChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) rejoined\\.")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]

            +PlayerConnectionStatusEvent.Rejoined(event, player)
        }
    }

    private object AreaOpenedChatHandler : AbstractChatHandler(
        Regex("([a-zA-Z0-9_]{3,16}) opened (.+)!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]
            val location = matchResult.groupValues[2]

            +AreaOpenedEvent(event, player, location)
        }
    }

    private object ItemPurchasedChatHandler : AbstractChatHandler(
        Regex("You purchased (.+)!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val item = matchResult.groupValues[1]

            +ItemPurchasedEvent(event, item)
        }
    }

    private object HitTargetChatHandler : AbstractChatHandler(
        Regex(" ⊕ ([a-zA-Z0-9_]{3,16}) hit the target!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val player = matchResult.groupValues[1]

            +TargetHitEvent(event, player)
        }
    }

    private object GoldReceivedChatHandler : AbstractChatHandler(
        Regex("\\+(\\d+) Gold(?: \\((.+)\\)|)")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            val matchResult = event.matchResult() ?: return
            val gold = matchResult.groupValues[1].toInt()

            +GoldReceivedEvent(event, gold)
        }
    }

    private object WindowRepairStartChatHandler : AbstractChatHandler(
        Regex("Repairing windows\\. Keep holding SNEAK to continue repairing\\.")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.Start(event)
        }
    }

    private object WindowRepairStopChatHandler1 : AbstractChatHandler(
        Regex("Stopped repairing\\. There are enemies nearby!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.Stop(event)
        }
    }

    private object WindowRepairStopChatHandler2 : AbstractChatHandler(
        Regex("Stopped repairing\\. Stay within range of the window to repair it!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.Stop(event)
        }
    }

    private object WindowRepairStopChatHandler3 : AbstractChatHandler(
        Regex("Stopped repairing\\. Hold SNEAK to continue repairing!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.Stop(event)
        }
    }

    private object WindowRepairEnemyNearbyChatHandler : AbstractChatHandler(
        Regex("You can't repair windows while enemies are nearby!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.EnemyNearby(event)
        }
    }

    private object WindowRepairFinishChatHandler : AbstractChatHandler(
        Regex("You have fully repaired this window!")
    ) {
        override fun handle(event: ChatReceivedEvent) {
            +WindowRepairEvent.Finish(event)
        }
    }
}