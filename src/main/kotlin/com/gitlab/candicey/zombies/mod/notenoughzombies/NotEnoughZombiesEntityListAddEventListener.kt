package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zombies.util.ZombiesUtil
import net.weavemc.loader.api.event.EntityListEvent
import net.weavemc.loader.api.event.SubscribeEvent

object NotEnoughZombiesEntityListAddEventListener {
    @SubscribeEvent
    fun onEntityListAddEvent(event: EntityListEvent.Add) {
        if (!ZombiesUtil.isEnabled()) {
            return
        }

        val entity = event.entity
        // TODO Add giant detection
    }
}