package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zenithcore.util.FieldCache
import com.gitlab.candicey.zenithcore.util.reflectField
import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.extension.reflectionToString
import com.gitlab.candicey.zombies.hook.NotEnoughZombiesConfigHook
import com.gitlab.candicey.zombies.sendNotification
import net.weavemc.loader.api.event.Event
import net.weavemc.loader.api.event.SubscribeEvent
import java.util.concurrent.ConcurrentHashMap

object NotEnoughZombiesEventDebugger {
    private val eventFields = ConcurrentHashMap<Class<*>, FieldCache?>()

    @SubscribeEvent
    fun onEvent(event: Event) {
        if (!NotEnoughZombiesConfig.debugEvents) {
            return
        }

        val eventClass = event::class.java
        val configEventField = getEventField(eventClass) ?: return
        if (configEventField[NotEnoughZombiesConfig] == true) {
            sendNotification(
                "Event Debugger",
                event.reflectionToString(),
                NotEnoughZombiesConfig.debugEventnotificationDuration
            )
        }
    }

    private fun getEventField(eventClass: Class<*>): FieldCache? =
        eventFields.getOrPut(eventClass) getOrPut@ {
            val fieldName = NotEnoughZombiesConfigHook.entries[eventClass] ?: return@getOrPut null
            NotEnoughZombiesConfig.reflectField(fieldName)
        }
}