package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zenithcore.util.Cache
import com.gitlab.candicey.zombies.data.PowerUp
import com.gitlab.candicey.zombies.event.LivingUpdateEvent
import com.gitlab.candicey.zombies.event.PowerUpDespawnEvent
import com.gitlab.candicey.zombies.event.PowerUpSpawnEvent
import com.gitlab.candicey.zombies.extension.unaryPlus
import com.gitlab.candicey.zombies.util.ZombiesUtil
import com.gitlab.candicey.zombies.warn
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityArmorStand
import net.minecraft.entity.monster.EntityZombie
import net.weavemc.loader.api.event.SubscribeEvent
import org.apache.commons.lang3.text.WordUtils
import java.util.*
import java.util.regex.Pattern

object NotEnoughZombiesLivingUpdateEventListener {
    private const val NAME_SEPARATOR = "\uD83C\uDF82" // "🎂"

    private val ignoredEntities = listOf(
        "Armor Stand",
        "§c§lTarget Practice",
        "§e§lHOLD SNEAK TO REVIVE!",
        "§e■■■■■■■■■■■■■■■",
    )

    private val reviveSecondsPattern = Pattern.compile("§c\\d+\\.\\d+s")

    private val entityNameCache = Cache<UUID, String>(
        cacheTime = Long.MAX_VALUE,
        maxCacheSize = 200
    )

    private val armourStandNameHandlers = listOf(
        PowerUpEntityArmourStandNameHandler,
    )

    @SubscribeEvent
    fun onLivingUpdateEvent(event: LivingUpdateEvent.Pre) {
        if (!ZombiesUtil.isEnabled()) {
            return
        }

        val entity = event.entity
        if (entity !is EntityArmorStand && entity !is EntityZombie) {
            return
        }

        val name = entity.name
        if (name in ignoredEntities) {
            return
        }

        if (reviveSecondsPattern.matcher(name).matches()) {
            return
        }

        when (entity) {
            is EntityArmorStand -> nameChange(entity)
        }
    }

    private fun nameChange(entity: EntityLivingBase) {
        var name = entity.name
        if (name.contains(NAME_SEPARATOR)) {
            name = name.substring(0, name.indexOf(NAME_SEPARATOR))
        }

        val uuid = entity.uniqueID
        val oldName = entityNameCache.get(uuid)
        if (name == oldName) {
            return
        }

        entityNameCache.store(uuid, name)

        when (entity) {
            is EntityArmorStand -> handleArmourStandEntity(entity, name)
        }
    }

    private fun handleArmourStandEntity(entity: EntityLivingBase, name: String) {
        armourStandNameHandlers
            .filter { it.regex.matches(name) }
            .forEach { it.handle(entity, name) }
    }

    private abstract class AbstractArmourStandNameHandler(val regex: Regex) {
        abstract fun handle(entity: EntityLivingBase, name: String)

        open fun EntityLivingBase.matchResult() = regex.find(name)
    }

    private object PowerUpEntityArmourStandNameHandler : AbstractArmourStandNameHandler(
        Regex("(?:§.)*(SHOPPING SPREE|DOUBLE GOLD|MAX AMMO|INSTA KILL|CARPENTER|BONUS GOLD)")
    ) {
        override fun handle(entity: EntityLivingBase, name: String) {
            val matchResult = entity.matchResult() ?: return
            val powerUpRaw = WordUtils.capitalize(matchResult.groupValues[1].lowercase())
            val powerUp = PowerUp.fromName(powerUpRaw)
            if (powerUp == null) {
                warn("Power up not found for entity=$entity, powerUp=$powerUpRaw")
                return
            }

            if (name.startsWith("§f")) {
                +PowerUpDespawnEvent(entity, powerUp)
            } else {
                +PowerUpSpawnEvent(entity, powerUp)
            }
        }
    }
}