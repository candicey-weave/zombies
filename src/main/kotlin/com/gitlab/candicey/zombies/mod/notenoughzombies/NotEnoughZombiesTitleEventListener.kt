package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zombies.event.GameOverEvent
import com.gitlab.candicey.zombies.event.GameStartEvent
import com.gitlab.candicey.zombies.event.RoundNewEvent
import com.gitlab.candicey.zombies.event.TitleEvent
import com.gitlab.candicey.zombies.extension.unaryPlus
import com.gitlab.candicey.zombies.util.ZombiesUtil
import net.weavemc.loader.api.event.SubscribeEvent

object NotEnoughZombiesTitleEventListener {
    private val titleHandlers = listOf(
        RoundNewTitleHandler,
        GameOverTitleHandler,
    )

    @SubscribeEvent
    fun onTitleEvent(event: TitleEvent) {
        if (!ZombiesUtil.isEnabled()) {
            return
        }

        titleHandlers
            .filter { it.titleRegex.matches(event.title) && it.subtitleRegex.matches(event.subtitle) }
            .forEach { it.handle(event) }
    }

    private abstract class AbstractTitleHandler(val titleRegex: Regex, val subtitleRegex: Regex) {
        abstract fun handle(event: TitleEvent)
    }

    private object RoundNewTitleHandler : AbstractTitleHandler(
        Regex("§cRound (\\d*)§r"),
        Regex("§r")
    ) {
        override fun handle(event: TitleEvent) {
            val round = titleRegex.find(event.title)?.groupValues?.get(1)?.toIntOrNull() ?: return

            +RoundNewEvent(round)

            if (round == 1) {
                +GameStartEvent
            }
        }
    }

    private object GameOverTitleHandler : AbstractTitleHandler(
        Regex("§cGame Over!§r"),
        Regex("§7You made it to Round (\\d*)!§r")
    ) {
        override fun handle(event: TitleEvent) {
            val finalRound = subtitleRegex.find(event.subtitle)?.groupValues?.get(1)?.toIntOrNull() ?: return

            +GameOverEvent(finalRound)
        }
    }
}