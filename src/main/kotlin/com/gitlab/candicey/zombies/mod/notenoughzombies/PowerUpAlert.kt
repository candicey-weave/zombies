package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.data.ChatOutput
import com.gitlab.candicey.zombies.event.PowerUpDespawnEvent
import com.gitlab.candicey.zombies.event.PowerUpSpawnEvent
import com.gitlab.candicey.zombies.extension.addNEZPrefix
import com.gitlab.candicey.zombies.extension.message
import net.weavemc.loader.api.event.SubscribeEvent
import java.util.*

object PowerUpAlert {
    private val spawnUUIDs = mutableSetOf<UUID>()

    private val despawnUUIDs = mutableSetOf<UUID>()

    @SubscribeEvent
    fun onPowerUpDespawnEvent(powerUpDespawnEvent: PowerUpDespawnEvent) {
        val uuid = powerUpDespawnEvent.entity.uniqueID
        if (despawnUUIDs.contains(uuid)) {
            return
        }

        despawnUUIDs.add(uuid)

        val powerUpName: String = powerUpDespawnEvent.powerUp.name
        "$powerUpName is going to despawn soon."
            .message(
                chatOutput = ChatOutput.fromIndex(NotEnoughZombiesConfig.powerUpDeSpawnAlert),
                prefix = String::addNEZPrefix
            )
    }

    @SubscribeEvent
    fun onPowerUpSpawnEvent(powerUpSpawnEvent: PowerUpSpawnEvent) {
        val uuid = powerUpSpawnEvent.entity.uniqueID
        if (spawnUUIDs.contains(uuid)) {
            return
        }

        spawnUUIDs.add(uuid)

        val powerUpName = powerUpSpawnEvent.powerUp.fullName
        "$powerUpName spawned."
            .message(
                chatOutput = ChatOutput.fromIndex(NotEnoughZombiesConfig.powerUpSpawnAlert),
                prefix = String::addNEZPrefix
            )
    }
}
