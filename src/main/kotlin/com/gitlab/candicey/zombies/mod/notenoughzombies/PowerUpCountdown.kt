package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zenithcore.util.Cache
import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.event.PowerUpSpawnEvent
import net.minecraft.entity.EntityLivingBase
import net.minecraft.entity.item.EntityArmorStand
import net.weavemc.loader.api.event.RenderLivingEvent
import net.weavemc.loader.api.event.SubscribeEvent
import java.util.*

object PowerUpCountdown {
    private const val NAME_SEPARATOR = "\uD83C\uDF82" // "🎂";
    private const val POWER_UP_DESPAWN_SECONDS = 60

    private val entityDespawnTimestampCache = Cache<UUID?, Long?>(
        cacheTime = Long.MAX_VALUE,
        maxCacheSize = 200,
    )

    @SubscribeEvent
    fun onRenderLivingEvent(renderLivingEvent: RenderLivingEvent.Pre) {
        if (!NotEnoughZombiesConfig.powerUpCountdown) {
            return
        }

        val entity = renderLivingEvent.entity
        if (entity !is EntityArmorStand) {
            return
        }

        val uuid = entity.uniqueID
        val entityDespawnTimestamp = entityDespawnTimestampCache.get(uuid) ?: return

        val currentTime = System.currentTimeMillis()
        if (currentTime % 10 != 0L) {
            return
        }

        val diff = entityDespawnTimestamp - currentTime

        if (diff <= 0) {
            entity.setAlwaysRenderNameTag(false)
            return
        }

        val newName = nameGenerator(entity, diff)

        entity.setCustomNameTag(newName)
    }

    @SubscribeEvent
    fun onPowerUpSpawnEvent(powerUpSpawnEvent: PowerUpSpawnEvent) {
        val entity = powerUpSpawnEvent.entity
        val uuid = entity.uniqueID

        val entityDespawnTimestamp = entityDespawnTimestampCache.get(uuid)
        if (entityDespawnTimestamp == null) {
            entityDespawnTimestampCache.store(uuid, System.currentTimeMillis() + POWER_UP_DESPAWN_SECONDS * 1000)
        }
    }

    private fun diffToString(diff: Long): String {
        val seconds = diff / 1000.0 // Convert milliseconds to seconds
        val secondsInt = (seconds * 10).toInt() // Multiply by 10 to keep one decimal place
        val formattedSeconds = secondsInt.toDouble() / 10 // Divide by 10 to get back to one decimal place
        return formattedSeconds.toString() + "s"
    }

    private fun nameGenerator(entity: EntityLivingBase, diff: Long): String {
        val suffix = NAME_SEPARATOR + " - " + diffToString(diff)
        val name = entity.name

        return if (name.contains(NAME_SEPARATOR)) {
            val originalName = name.split(NAME_SEPARATOR)[0]
            originalName + suffix
        } else {
            name + suffix
        }
    }
}
