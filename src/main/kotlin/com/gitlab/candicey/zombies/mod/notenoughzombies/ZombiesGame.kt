package com.gitlab.candicey.zombies.mod.notenoughzombies

import com.gitlab.candicey.zenithcore.extension.addChatMessage
import com.gitlab.candicey.zenithcore.extension.toChatComponent
import com.gitlab.candicey.zombies.command.impl.advancedDebugOperation
import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.data.ChatOutput
import com.gitlab.candicey.zombies.data.Map
import com.gitlab.candicey.zombies.data.PowerUp
import com.gitlab.candicey.zombies.event.*
import com.gitlab.candicey.zombies.extension.addNEZPrefix
import com.gitlab.candicey.zombies.extension.message
import com.gitlab.candicey.zombies.sendNotification
import com.gitlab.candicey.zombies.util.ZombiesUtil
import com.gitlab.candicey.zombies.warn
import net.weavemc.loader.api.event.SubscribeEvent

object ZombiesGame {
    val luckyChestRollData = mutableMapOf<String, kotlin.collections.Map<String, Int>>()

    val powerUpPatternMap = mutableMapOf<PowerUp, Int>()

    var map: Map? = null

    var currentRound = -1

    var isInGame = false

    @SubscribeEvent
    fun onGameStartEvent(event: GameStartEvent) {
        clear()
        map = ZombiesUtil.getMap()
        currentRound = 1
        isInGame = true
    }

    @SubscribeEvent
    fun onRoundNewEvent(event: RoundNewEvent) {
        if (map == null) {
            map = ZombiesUtil.getMap()
        }

        currentRound = event.round
        val powerUpString = StringBuilder()
        val allSavedPowerUps = getAllSavedPowerUps()
        if (allSavedPowerUps.isEmpty()) {
            "No power up data."
                .addNEZPrefix()
                .toChatComponent()
                .addChatMessage()
            return
        }

        var c = 0
        for (powerUp in allSavedPowerUps) {
            val nextRoundWithPowerUp: Int = getNextPowerUpRound(powerUp) ?: -1
            powerUpString
                .append(powerUp.shortName)
                .append(": ")
                .append(nextRoundWithPowerUp)
            if (c >= allSavedPowerUps.size - 1) {
                break
            }
            powerUpString.append(", ")
            c++
        }

        "Next power up rounds; $powerUpString."
            .message(
                chatOutput = ChatOutput.fromIndex(NotEnoughZombiesConfig.nextPowerUpRoundAlert),
                prefix = String::addNEZPrefix
            )
    }

    @SubscribeEvent
    fun onPowerUpSpawnEvent(event: PowerUpSpawnEvent) {
        val powerUp = event.powerUp
        val map = getMap() ?: return
        if (!powerUp.hasPattern(map)) {
            return
        }
        if (powerUpPatternExists(powerUp)) {
            return
        }

        val patternNum = powerUp.getPatternNumber(map, currentRound)
        if (patternNum == null) {
            advancedDebugOperation {
                warn("PowerUp pattern not found. PowerUp: ${powerUp.fullName} round: $currentRound")
            }
            return
        }

        powerUpPatternMap[powerUp] = patternNum
    }

    @SubscribeEvent
    fun onLuckyChestEvent(event: LuckyChestEvent) {
        val player = event.player
        val item = event.item

        val playerItems = luckyChestRollData.getOrPut(player) { mutableMapOf() } as MutableMap

        playerItems.putIfAbsent(item, 0)
        playerItems[item] = playerItems[item]!! + 1
    }

    @SubscribeEvent
    fun onGameOverEvent(event: GameOverEvent) {
        clear()
    }

    private fun getMap(notifyNull: Boolean = true): Map? {
        if (map == null) {
            map = ZombiesUtil.getMap()
        }

        if (map == null && notifyNull) {
            sendNotification(
                "Not Enough Zombies",
                "Could not find the current map."
            )
        }

        return map
    }

    private fun clear() {
        luckyChestRollData.clear()
        powerUpPatternMap.clear()
        map = null
        currentRound = -1
        isInGame = false
    }

    private fun getPatternNum(powerUp: PowerUp): Int? {
        val map = getMap() ?: return null
        return if (!powerUp.hasPattern(map)) {
            null
        } else {
            powerUpPatternMap[powerUp]
        }
    }

    private fun getNextPowerUpRound(powerUp: PowerUp): Int? {
        val patternNum = getPatternNum(powerUp) ?: return null
        return powerUp.getNextPowerUpRound(getMap() ?: return null, currentRound, patternNum)
    }

    private fun getAllSavedPowerUps(): Set<PowerUp> = powerUpPatternMap.keys

    private fun powerUpPatternExists(powerUp: PowerUp): Boolean = powerUpPatternMap[powerUp] != null

    fun setPowerUpPattern(powerUp: PowerUp, pattern: Int) {
        if (!isInGame) {
            return
        }

        val mapPattern = powerUp.getPattern(getMap() ?: return) ?: return

        val numberOfPatterns = mapPattern.size
        if (pattern !in 0 until numberOfPatterns) {
            return
        }

        powerUpPatternMap[powerUp] = pattern
        "${powerUp.name} pattern set to $pattern."
            .addNEZPrefix()
            .toChatComponent()
            .addChatMessage()
    }

    fun resetPowerUpPattern(powerUp: PowerUp) {
        if (!isInGame) {
            return
        }

        powerUpPatternMap.remove(powerUp)

        "${powerUp.name} was reset."
            .addNEZPrefix()
            .toChatComponent()
            .addChatMessage()
    }
}