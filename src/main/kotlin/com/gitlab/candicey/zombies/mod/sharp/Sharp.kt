package com.gitlab.candicey.zombies.mod.sharp

import com.gitlab.candicey.zombies.config.sharpConfig
import com.gitlab.candicey.zombies.util.ZombiesUtil
import net.minecraft.entity.Entity
import net.minecraft.entity.EntityLiving
import net.weavemc.loader.api.event.RenderLivingEvent
import net.weavemc.loader.api.event.SubscribeEvent
import org.lwjgl.opengl.GL11.*

object Sharp {
    @SubscribeEvent
    fun onRenderLivingEventPre(event: RenderLivingEvent.Pre) = with(sharpConfig) {
        if (this == null) {
            return
        }

        if (!enabled) {
            return
        }

        val entity = event.entity

        if (!shouldRender(entity)) {
            return
        }

        glEnable(GL_POLYGON_OFFSET_FILL)
        glPolygonOffset(1.0f, -1000000f)
    }

    @SubscribeEvent
    fun onRenderLivingEventPost(event: RenderLivingEvent.Post) = with(sharpConfig) {
        if (this == null) {
            return
        }

        if (!enabled) {
            return
        }

        val entity = event.entity

        if (!shouldRender(entity)) {
            return
        }

        glPolygonOffset(1.0f, 1000000f)
        glDisable(GL_POLYGON_OFFSET_FILL)
    }

    private fun shouldRender(entity: Entity): Boolean = entity is EntityLiving && !entity.isInvisible && ZombiesUtil.isZombiesGame()
}