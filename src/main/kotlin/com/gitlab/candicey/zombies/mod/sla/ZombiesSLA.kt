package com.gitlab.candicey.zombies.mod.sla

import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.eventbus.Subscribe
import cc.polyfrost.oneconfig.utils.dsl.nanoVG
import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zombies.config.ZombiesSlaConfig
import com.gitlab.candicey.zombies.data.Map
import com.gitlab.candicey.zombies.data.Room
import com.gitlab.candicey.zombies.enum.RotationDirection
import com.gitlab.candicey.zombies.event.GameStartEvent
import com.gitlab.candicey.zombies.extension.drawLines
import com.gitlab.candicey.zombies.extension.shouldAlignBottom
import com.gitlab.candicey.zombies.sendNotification
import com.gitlab.candicey.zombies.util.CornerPositionUtil
import com.gitlab.candicey.zombies.util.ZombiesUtil
import net.weavemc.loader.api.event.SubscribeEvent

var sla: ZombiesSLA? = null
    private set

class ZombiesSLA(map: Map) {
    private var rooms: List<Room>

    init {
        rooms = when (map) {
            Map.DEAD_END -> Room.deadEnd
            Map.BAD_BLOOD -> Room.badBlood
            Map.ALIEN_ARCADIUM -> Room.alienArcadium
        }
    }

    fun refreshActives() {
        val player = mc.thePlayer
        val playerCoordsX = player.posX + ZombiesSlaConfig.offsetX
        val playerCoordsY = player.posY + ZombiesSlaConfig.offsetY
        val playerCoordsZ = player.posZ + ZombiesSlaConfig.offsetZ

        for (room in rooms) {
            room.resetActiveWindowCount()
            for (window in room.windows) {
                val distanceDoubledThenSquared =
                    (playerCoordsX * 2 - window.x) * (playerCoordsX * 2 - window.x) +
                    (playerCoordsY * 2 - window.y) * (playerCoordsY * 2 - window.y) +
                    (playerCoordsZ * 2 - window.z) * (playerCoordsZ * 2 - window.z)

                window.isActive = distanceDoubledThenSquared < 10000
                if (window.isActive) {
                    room.increaseActiveWindowCount()
                }
            }
        }
    }

    fun rotate(direction: RotationDirection) {
        for (room in rooms) {
            for (window in room.windows) {
                window.rotate(direction)
            }
        }
    }

    fun rotate(rotationCount: Int) {
        for (room in rooms) {
            for (window in room.windows) {
                window.rotate(rotationCount)
            }
        }
    }

    fun mirrorX() {
        for (room in rooms) {
            for (window in room.windows) {
                window.mirrorX()
            }
        }
    }

    fun mirrorZ() {
        for (room in rooms) {
            for (window in room.windows) {
                window.mirrorZ()
            }
        }
    }

    companion object Handler {
        @Subscribe
        fun onHudRenderEvent(event: HudRenderEvent) = with(ZombiesSlaConfig) {
            if (!enabled) {
                return
            }

            val sla = sla ?: return

            sla.refreshActives()

            val rooms = sla.rooms

            nanoVG {
                val position = CornerPositionUtil.fromIndex(position)

                val roomStringProvider = func@ { yIndex: Int ->
                    if (yIndex == rooms.size) {
                        return@func null
                    }

                    val room = rooms[if (position.shouldAlignBottom()) rooms.size - yIndex - 1 else yIndex]
                    val actives = room.activeWindowCount

                    if (actives == 0) {
                        ""
                    } else {
                        String.format("%s: %x", room.alias, actives)
                    }
                }

                drawLines(
                    lineProvider = roomStringProvider,
                    colourProvider = { textColour },
                    size = fontSize.toFloat(),
                    corner = position,
                    marginX = textMarginX.toFloat(),
                    marginY = textMarginY.toFloat(),
                    shadowProvider = { textShadow },
                )
            }
        }

        @SubscribeEvent
        fun onGameStartEvent(event: GameStartEvent) {
            val map = ZombiesUtil.getMap()
            if (map == null) {
                sendNotification(
                    "Zombies SLA",
                    "Could not find the current map."
                )
                return
            }

            changeMap(map)

            sendNotification("Zombies SLA", "Changed map to ${map.mapName}.")
        }

        fun changeMap(map: Map) {
            sla = ZombiesSLA(map)
        }

        fun clearMap() {
            sla = null
        }
    }
}