package com.gitlab.candicey.zombies.mod.strategyviewer

import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.eventbus.Subscribe
import cc.polyfrost.oneconfig.renderer.font.Fonts
import cc.polyfrost.oneconfig.utils.dsl.drawRoundedRect
import cc.polyfrost.oneconfig.utils.dsl.drawText
import cc.polyfrost.oneconfig.utils.dsl.getTextWidth
import cc.polyfrost.oneconfig.utils.dsl.nanoVG
import com.gitlab.candicey.zenithcore.extension.delete
import com.gitlab.candicey.zombies.config.ZombiesStrategyViewerConfig
import com.gitlab.candicey.zombies.data.LineTypes
import com.gitlab.candicey.zombies.extension.drawTextRightAligned
import com.gitlab.candicey.zombies.extension.drawTextRightAlignedWithShadow
import com.gitlab.candicey.zombies.extension.drawTextWithShadow
import com.gitlab.candicey.zombies.sendNotification
import org.apache.commons.lang3.text.WordUtils
import java.net.URL

object ZombiesStrategyViewer {
    var renderedText = mutableListOf<LineTypes>()

    var currentLine = 0

    val maximumCurrentLine: Int
        get() = (currentLine + ZombiesStrategyViewerConfig.showLineCount).coerceAtMost(renderedText.size)

    // Pair<isSuccessful, text>
    var cacheText = Pair(false, "")

    var firstlyCalculated = false

    @Subscribe
    fun onHudRenderEvent(event: HudRenderEvent) {
        if (!ZombiesStrategyViewerConfig.enabled) {
            return
        }

        val renderedTextClone = renderedText

        if (renderedTextClone.isEmpty()) {
            return
        }

        val splicedText = renderedTextClone.slice(currentLine.coerceAtMost(renderedTextClone.size - 1) until maximumCurrentLine)

        val fontSize = ZombiesStrategyViewerConfig.fontSize.toFloat()
        val font = Fonts.REGULAR

        val padding = fontSize / 1.65f
        val x = padding * 1.3f
        var y = 2 * x

        var hasWritten = false
        var writtenLines = 0

        val textList = mutableListOf<Pair<String, Float>>()

        for ((index, lineTypes) in splicedText.withIndex()) {
            if (writtenLines >= ZombiesStrategyViewerConfig.showLineCount) {
                break
            }

            when (lineTypes) {
                is LineTypes.Empty -> {
                    if (hasWritten) {
                        writtenLines++
                        if (!checkIfUntilEndLinesAreEmpty(index, splicedText)) {
                            y += fontSize * ((ZombiesStrategyViewerConfig.newLineHeight * 1.3f - 100) / 100 + 1)
                        }
                    }
                }

                is LineTypes.NewLine -> {
                    if (hasWritten) {
                        writtenLines++
                        if (!checkIfUntilEndLinesAreEmpty(index, splicedText)) {
                            y += fontSize * ((ZombiesStrategyViewerConfig.newLineHeight * 1.3f - 100)  / 100f + 1)
                        }
                    }
                }

                is LineTypes.Text -> {
                    if (!hasWritten) {
                        hasWritten = true
                    }

                    writtenLines++
                    textList.add(Pair(lineTypes.text, y))
                    y += if (checkIfUntilEndLinesAreEmpty(index, splicedText)) {
                        fontSize
                    } else {
                        fontSize * 1.08f
                    }
                }
            }
        }

        nanoVG {
            val widest = textList.maxOfOrNull { getTextWidth(it.first, fontSize, font) } ?: 0f

            drawRoundedRect(
                x - padding / 1.6f,
                padding,
                widest + 2 * x + padding,
                y + padding,
                ZombiesStrategyViewerConfig.backgroundCornerRadius,
                ZombiesStrategyViewerConfig.backgroundColour.rgb
            )

            val shadowOffset = fontSize / 15
            for ((text, yPos) in textList) {
                if (ZombiesStrategyViewerConfig.textShadow) {
                    drawTextWithShadow(
                        text,
                        x + padding,
                        yPos + padding,
                        ZombiesStrategyViewerConfig.textColour,
                        fontSize,
                        font,
                        shadowOffset,
                        0.2f
                    )
                } else {
                    drawText(
                        text,
                        x + padding,
                        yPos + padding,
                        ZombiesStrategyViewerConfig.textColour.rgb,
                        fontSize,
                        font
                    )
                }
            }

            if (ZombiesStrategyViewerConfig.lineCountShadow) {
                drawTextRightAlignedWithShadow(
                    "${(currentLine + 1).coerceAtMost(renderedTextClone.size)}/${renderedTextClone.size}",
                    widest + 2 * x + padding * 0.7f,
                    y + padding,
                    ZombiesStrategyViewerConfig.lineCountColour,
                    fontSize / 1.5f,
                    font,
                    shadowOffset,
                    0.2f
                )
            } else {
                drawTextRightAligned(
                    "${(currentLine + 1).coerceAtMost(renderedTextClone.size)}/${renderedTextClone.size}",
                    widest + 2 * x + padding * 0.7f,
                    y + padding,
                    ZombiesStrategyViewerConfig.lineCountColour.rgb,
                    fontSize / 1.5f,
                    font
                )
            }
        }
    }

    fun fetchText(notifyError: Boolean = true): Pair<Boolean, String> {
        return try {
            true to URL(ZombiesStrategyViewerConfig.strategy).openStream().bufferedReader().use { it.readText() }
        } catch (e: Exception) {
            e.printStackTrace()

            if (notifyError && firstlyCalculated) {
                sendNotification(
                    "Zombies Strategy Viewer",
                    "Failed to fetch strategy from ${ZombiesStrategyViewerConfig.strategy}."
                )
            }

            false to "Error: ${e.stackTrace.joinToString("\n")}"
        }
    }

    fun refreshCachedText(notifyError: Boolean = false) {
        cacheText = fetchText(notifyError)
    }

    fun calculateLines(notifyError: Boolean = false, notifySuccess: Boolean = false, ignoreFirstlyCalculatedTextIsBlankNotification: Boolean = false, refreshCachedText: Boolean = true) {
        if (!ZombiesStrategyViewerConfig.enabled) {
            return
        }

        val list = mutableListOf<LineTypes>()

        if (refreshCachedText) {
            if (ZombiesStrategyViewerConfig.strategy.isBlank()) {
                renderedText = list

                if (firstlyCalculated || ignoreFirstlyCalculatedTextIsBlankNotification) {
                    sendNotification(
                        "Zombies Strategy Viewer",
                        "Strategy URL is blank."
                    )
                }

                return
            }

            refreshCachedText(notifyError)
        }

        val (isSuccessful, text) = cacheText

        text.split("\n")
            .map { it.delete("\r") }
            .also {
                if ((notifySuccess && isSuccessful && firstlyCalculated) || ignoreFirstlyCalculatedTextIsBlankNotification) {
                    sendNotification(
                        "Zombies Strategy Viewer",
                        "Successfully fetched strategy. (${it.size} lines)"
                    )
                }
            }
            .forEach { line ->
                if (line.isBlank()) {
                    list.add(LineTypes.Empty)
                } else if (line.length > ZombiesStrategyViewerConfig.maximumCharactersPerLine) {
                    WordUtils.wrap(line, ZombiesStrategyViewerConfig.maximumCharactersPerLine)
                        .split("\n")
                        .forEach { splitLine ->
                            list.add(LineTypes.Text(splitLine))
                        }
                    list.add(LineTypes.NewLine)
                } else {
                    list.add(LineTypes.Text(line))
                    list.add(LineTypes.NewLine)
                }
            }

        renderedText = list

        if (!firstlyCalculated) {
            firstlyCalculated = true
        }
    }

    private fun checkIfUntilEndLinesAreEmpty(currentIndex: Int, list: List<LineTypes>): Boolean {
        return list.slice(currentIndex until list.size).all { it is LineTypes.Empty || it is LineTypes.NewLine }
    }
}