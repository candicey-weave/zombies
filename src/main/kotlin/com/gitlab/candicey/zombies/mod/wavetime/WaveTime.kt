package com.gitlab.candicey.zombies.mod.wavetime

import cc.polyfrost.oneconfig.events.event.HudRenderEvent
import cc.polyfrost.oneconfig.libs.eventbus.Subscribe
import cc.polyfrost.oneconfig.utils.dsl.nanoVG
import com.gitlab.candicey.zombies.config.WaveTimeConfig
import com.gitlab.candicey.zombies.data.Wave
import com.gitlab.candicey.zombies.extension.drawLines
import com.gitlab.candicey.zombies.extension.indexOfFirstOrNull
import com.gitlab.candicey.zombies.extension.shouldAlignBottom
import com.gitlab.candicey.zombies.mod.autosplits.AutoSplits
import com.gitlab.candicey.zombies.mod.autosplits.splitter.InternalSplitter
import com.gitlab.candicey.zombies.util.CornerPositionUtil
import com.gitlab.candicey.zombies.util.ScoreboardUtil

object WaveTime {
    @Subscribe
    fun onHudRenderEvent(event: HudRenderEvent) = with(WaveTimeConfig) {
        if (!enabled) {
            return
        }

        val internalSplitter = AutoSplits.splitter as? InternalSplitter ?: return

        nanoVG {
            val milliseconds = internalSplitter.milliseconds
            val seconds = milliseconds / 1000

            val position = CornerPositionUtil.fromIndex(position)

            val map = ScoreboardUtil.getMap() ?: return@nanoVG
            val round = ScoreboardUtil.getRound() ?: return@nanoVG
            val waveData = Wave.getWaveData(map, round)

            // find the active wave (1-indexed)
            // for example, we have waveData = {10, 20, 37}
            // if seconds is 15, then we are in wave 1
            // if seconds is 2, then we are in wave 0
            val activeWave = waveData.indexOfFirstOrNull { seconds < it } ?: waveData.size

            fun getIndex(yIndex: Int) = if (position.shouldAlignBottom()) waveData.size - yIndex - 1 else yIndex

            val waveStringProvider = func@ { yIndex: Int ->
                if (yIndex == waveData.size) {
                    return@func null
                }

                val index = getIndex(yIndex)
                val waveStartTime = waveData[index]
                val startTimeMinute = waveStartTime / 60
                val startTimeSecond = waveStartTime % 60

                String.format("W%d: %02d:%02d", index + 1, startTimeMinute, startTimeSecond)
            }
            val colourProvider = { yIndex: Int ->
                if (activeWave == getIndex(yIndex) + 1) {
                    activeWaveTextColour
                } else {
                    inactiveWaveTextColour
                }
            }
            val shadowProvider = { yIndex: Int ->
                if (activeWave == getIndex(yIndex) + 1) {
                    activeWaveTextShadow
                } else {
                    inactiveWaveTextShadow
                }
            }

            drawLines(
                lineProvider = waveStringProvider,
                colourProvider = colourProvider,
                size = fontSize.toFloat(),
                corner = position,
                marginX = textMarginX.toFloat(),
                marginY = textMarginY.toFloat(),
                shadowProvider = shadowProvider,
            )
        }
    }
}