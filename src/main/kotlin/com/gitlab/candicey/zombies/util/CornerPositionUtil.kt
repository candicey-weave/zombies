package com.gitlab.candicey.zombies.util

import com.gitlab.candicey.zenithcore.enum.CornerPosition
import com.gitlab.candicey.zenithcore.enum.CornerPosition.*

object CornerPositionUtil {
    fun fromIndex(index: Int): CornerPosition =
        when (index) {
            0 -> TOP_LEFT
            1 -> TOP_RIGHT
            2 -> BOTTOM_LEFT
            3 -> BOTTOM_RIGHT
            else -> throw IllegalArgumentException("Invalid index: $index")
        }

    fun toIndex(cornerPosition: CornerPosition): Int =
        when (cornerPosition) {
            TOP_LEFT -> 0
            TOP_RIGHT -> 1
            BOTTOM_LEFT -> 2
            BOTTOM_RIGHT -> 3
        }
}