package com.gitlab.candicey.zombies.util

import com.gitlab.candicey.zenithcore.mc
import com.gitlab.candicey.zombies.data.Map
import net.minecraft.scoreboard.ScorePlayerTeam
import net.weavemc.loader.api.event.SubscribeEvent
import net.weavemc.loader.api.event.TickEvent
import java.util.regex.Pattern

object ScoreboardUtil {
    private val SIDEBAR_EMOJI_PATTERN = Pattern.compile("[\uD83D\uDD2B\uD83C\uDF6B\uD83D\uDCA3\uD83D\uDC7D\uD83D\uDD2E\uD83D\uDC0D\uD83D\uDC7E\uD83C\uDF20\uD83C\uDF6D\u26BD\uD83C\uDFC0\uD83D\uDC79\uD83C\uDF81\uD83C\uDF89\uD83C\uDF82]+")

    private val STRIP_COLOR_PATTERN = Pattern.compile("§[0-9A-FK-ORZ]", Pattern.CASE_INSENSITIVE)

    private val ROUND_LINE_PATTERN = Pattern.compile("(Round )([0-9]{1,3})")

    private val SERVER_NUMBER_PATTERN = Pattern.compile(".*([mLM][0-9A-Z]+)")

    private val MAP_PATTERN = Pattern.compile("Map:.*(Dead End|Bad Blood|Alien Arcadium)")

    private var title: String? = null

    private var lines: List<String>? = null

    @SubscribeEvent
    fun onTickEventPre(event: TickEvent.Pre) {
        refresh()
    }

    /**
     * Overrides [title] and [lines] if a valid scoreboard is present
     */
    fun refresh() {
        if (!ZombiesUtil.isZombiesGame()) {
            return
        }

        val scoreboard = mc.theWorld?.scoreboard ?: return
        val sidebar = scoreboard.getObjectiveInDisplaySlot(1) ?: return

        title = STRIP_COLOR_PATTERN.matcher(sidebar.displayName.trim { it <= ' ' }).replaceAll("")

        val scoreCollection = scoreboard.getSortedScores(sidebar)
        val filteredScores = scoreCollection.filter { it.playerName != null && !it.playerName.startsWith("#") }.toList()
        val scores = (if (filteredScores.size > 15) filteredScores.subList(filteredScores.size - 15, filteredScores.size) else filteredScores).reversed()

        lines = scores.map { score ->
            val team = scoreboard.getPlayersTeam(score.playerName)
            val scoreboardLine = ScorePlayerTeam.formatPlayerName(team, score.playerName).trim { it <= ' ' }
            STRIP_COLOR_PATTERN.matcher(SIDEBAR_EMOJI_PATTERN.matcher(scoreboardLine).replaceAll("")).replaceAll("")
        }
    }

    fun getRound(): Int? {
        val line = runCatching { lines!![2] }.getOrNull() ?: return null

        val roundString = ROUND_LINE_PATTERN.matcher(line).replaceAll("$2")

        return roundString.toIntOrNull()
    }

    fun getServerNumber(): String? {
        val line = runCatching { lines!![0] }.getOrNull() ?: return null

        return SERVER_NUMBER_PATTERN.matcher(line).replaceAll("$1")
    }

    fun getMap(): Map? {
        val line = runCatching { lines!![12] }.getOrNull() ?: runCatching { lines!![2] }.getOrNull() ?: return null

        val mapString = MAP_PATTERN.matcher(line).replaceAll("$1")

        return when (mapString) {
            "Dead End" -> Map.DEAD_END
            "Bad Blood" -> Map.BAD_BLOOD
            "Alien Arcadium" -> Map.ALIEN_ARCADIUM
            else -> null
        }
    }

    fun isZombies(): Boolean = "ZOMBIES" == title
}