package com.gitlab.candicey.zombies.util

import cc.polyfrost.oneconfig.utils.hypixel.HypixelUtils
import cc.polyfrost.oneconfig.utils.hypixel.LocrawUtil
import com.gitlab.candicey.zombies.config.NotEnoughZombiesConfig
import com.gitlab.candicey.zombies.data.Map

object ZombiesUtil {
    fun isEnabled(): Boolean = isZombiesGame() && NotEnoughZombiesConfig.enabled

    fun isZombiesGame(): Boolean =
        runCatching {
            HypixelUtils.INSTANCE.isHypixel()
                    && LocrawUtil.INSTANCE.locrawInfo!!.gameMode.startsWith("ZOMBIES_")
        }.getOrDefault(false)

    fun getMap(): Map? =
        if (!isZombiesGame()) {
            null
        } else {
            runCatching {
                Map.fromMapName(LocrawUtil.INSTANCE.locrawInfo!!.mapName)
            }.getOrNull()
        }
}